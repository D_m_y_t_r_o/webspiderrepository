﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SheduleTest
{
    class DataContext : DbContext
    {
        public DataContext() : base("name=DataMemorryConnectionString")
        {

        }
        public DbSet<Info> Infoes {get;set;} 
    }

}
