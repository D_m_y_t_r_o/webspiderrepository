﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SheduleTest
{
    public class Scheduler
    {
        private static Scheduler instance;
        private List<Timer> timers = new List<Timer>();

        private Scheduler() { }

        public static Scheduler Instance => instance ?? (instance = new Scheduler());

        public void Task(double interval, Action task)
        {
            Timer timer = new Timer(x =>
            {
                task.Invoke();
            }, null, TimeSpan.Zero, TimeSpan.FromHours(interval));
            timers.Add(timer);
        }
    }
}


