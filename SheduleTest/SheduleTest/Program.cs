﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using LinqKit;

namespace SheduleTest
{
    class Program
    {
        public static bool TasksDo= true;
        public static Queue<Info> InfoQueue = new Queue<Info>();
        static void Main(string[] args)
        {
            MyScheduler.Interval(1,
            () =>
            {
                if (TasksDo)
                {
                    TasksDo = false;
                    MyExtentions.ListToQueue(GetInfo(),ref InfoQueue);
                    TasksDo = true;

                    if (InfoQueue.Any())
                    {
                        //проверка
                        Console.WriteLine(55);
                    }
                }
                else
                {
                    return;
                }
            });

            Console.ReadLine();
        }


        public static List<Info> GetInfo()
        {
            var timeInSeconds = DateTime.Now.Hour * 60 + DateTime.Now.Minute;
            Func<Info, bool> myBoolPredicate = o =>
               (timeInSeconds - o.TimeStart.Hour * 60 + o.TimeStart.Minute) % o.Period >= 0 &&
               (timeInSeconds - o.TimeStart.Hour * 60 + o.TimeStart.Minute) % o.Period <= o.Period &&
               o.TimeStart.Date < DateTime.Now;

            using (DataContext db = new DataContext())
            {
                var resultlist = db.Infoes
                    .Where(myBoolPredicate)
                    .ToList();
                return resultlist;
            }
        }
    }
}