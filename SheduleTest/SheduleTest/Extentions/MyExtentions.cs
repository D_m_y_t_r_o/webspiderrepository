﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SheduleTest
{
    public static class MyExtentions
    {
        public static Queue<T> ListToQueue<T>(this List<T> original, ref Queue<Info> InfoQueue)
            where T : Info
        {
            Queue<T> result = new Queue<T>();
            foreach (var item in original)
            {
                InfoQueue.Enqueue(item);
            }
            return result;
        }
    }
}
