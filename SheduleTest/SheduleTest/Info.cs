﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SheduleTest
{
    public class Info
    {
        public int Id { get; set; }
        public string URL { get; set; }       
        public string Tag { get; set; }
        public int Period { get; set; }
        public DateTime TimeStart { get; set; }

    }
}
