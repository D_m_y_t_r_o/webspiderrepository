﻿using System;

namespace SheduleTest
{
    public static class MyScheduler
    {
        public static void Interval(double interval, Action task)
        {
            interval = interval / 3600;
            Scheduler.Instance.Task(interval, task);
        }
    }
}