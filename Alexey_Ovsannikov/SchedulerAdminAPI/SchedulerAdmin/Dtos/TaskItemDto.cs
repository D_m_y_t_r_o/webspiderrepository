﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchedulerAdmin.Dtos
{
    public class TaskItemDto
    {
        public int Id { get; set; } 
        public string TargetSite { get; set; } 
        public string XPath { get; set; } 
        public DateTime TimeStart { get; set; } 
        public TimeSpan? ExecuteFrequency { get; set; } 
        public string Username { get; set; }
    }
}
