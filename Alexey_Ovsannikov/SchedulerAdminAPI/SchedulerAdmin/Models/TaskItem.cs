﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SchedulerAdmin.Models
{
    public class TaskItem
    {
        public int Id { get; set; }
        [Required (ErrorMessage = "You must input site address")]
        [Column(TypeName = "nvarchar(150)")]
        public string TargetSite { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(100)")]
        public string XPath { get; set; }
        [Required] 
        public DateTime TimeStart { get; set; } 
        public TimeSpan? ExecuteFrequency { get; set; }
        [JsonIgnore]
        public virtual User User { get; set; }
    }
}
