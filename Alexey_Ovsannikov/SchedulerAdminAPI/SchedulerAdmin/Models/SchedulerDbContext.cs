﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore; 
using SchedulerAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchedulerAdmin.Models
{
    public class SchedulerDbContext : DbContext
    {
        public SchedulerDbContext(DbContextOptions<SchedulerDbContext> options) : base(options)
        {
        }

        public DbSet<TaskItem> TaskItems { get; set; }  
        public DbSet<User> Users { get; set; }
    }
}
