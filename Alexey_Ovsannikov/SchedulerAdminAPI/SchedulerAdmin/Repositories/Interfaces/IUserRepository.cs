﻿using SchedulerAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SchedulerAdmin.Repositories.Interfaces
{
    public interface IUserRepository
    {
        Task<IEnumerable<User>> GetAllAsync();
        Task<User> GetByIdAsync(int userId);
        Task<User> GetByFilterAsync(Func<User, bool> filter); 
        Task<User> CreateAsync(User user);
        User Delete(User user);
        Task<bool> SaveChangesAsync();
    }
}
