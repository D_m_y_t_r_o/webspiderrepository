﻿using SchedulerAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchedulerAdmin.Repositories.Interfaces
{
    public interface ITaskRepository
    {
        Task<IEnumerable<TaskItem>> GetAllAsync();
        Task<TaskItem> GetByIdAsync(int id);
        Task<IEnumerable<TaskItem>> GetTasksByUserName(string username);
        Task<TaskItem> CreateAsync(TaskItem taskItem);
        TaskItem Delete(TaskItem taskItem);
        TaskItem Update(int id, TaskItem taskItem);
        Task<bool> SaveChangesAsync();
        Task<int> CountAsync(); 
    }
}
