﻿using Microsoft.EntityFrameworkCore; 
using SchedulerAdmin.Models;
using SchedulerAdmin.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SchedulerAdmin.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        private readonly SchedulerDbContext _schedulerContext;

        public TaskRepository(SchedulerDbContext schedulerDbContext)
        {
            _schedulerContext = schedulerDbContext;
        }

        public IQueryable<TaskItem> GetTaskWithUsers =>
            _schedulerContext.TaskItems.Include(u => u.User);

        public async Task<IEnumerable<TaskItem>> GetAllAsync() => 
            await GetTaskWithUsers.ToListAsync();


        public async Task<TaskItem> GetByIdAsync(int id) =>
            await _schedulerContext.TaskItems.FirstOrDefaultAsync(x => x.Id == id);


        public async Task<IEnumerable<TaskItem>> GetTasksByUserName(string username)
        {
            IEnumerable<TaskItem> tasks = await GetTaskWithUsers.Where(task => task.User.Username == username).ToListAsync();
            return tasks;
        }

        public async Task<TaskItem> CreateAsync(TaskItem taskItem)
        { 
            var addedTask = await _schedulerContext.TaskItems.AddAsync(taskItem);

            return addedTask.Entity;
        }

        public TaskItem Delete(TaskItem taskItem)
        { 
            var deletedTask = _schedulerContext.TaskItems.Remove(taskItem);
             
            return deletedTask.Entity;
        }
        public async Task<int> CountAsync() => 
            await _schedulerContext.TaskItems.CountAsync(); 
         

        public TaskItem Update(int id, TaskItem taskItem)
        {
            var updatedTask = _schedulerContext.TaskItems.Update(taskItem);
             
            return updatedTask.Entity;
        }
        public async Task<bool> SaveChangesAsync() =>
            await _schedulerContext.SaveChangesAsync() > 0;
    }
}
