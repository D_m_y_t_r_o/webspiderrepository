﻿using Microsoft.EntityFrameworkCore;
using SchedulerAdmin.Models;
using SchedulerAdmin.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SchedulerAdmin.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly SchedulerDbContext _schedulerDbContext;

        public UserRepository(SchedulerDbContext schedulerDbContext)
        {
            _schedulerDbContext = schedulerDbContext;
        }

        public async Task<IEnumerable<User>> GetAllAsync() =>
            await _schedulerDbContext.Users.AsNoTracking().ToListAsync();

        public async Task<User> GetByIdAsync(int userId) =>
            await _schedulerDbContext.Users.FirstOrDefaultAsync(u => u.id == userId);
         
        public async Task<User> GetByFilterAsync(Func<User, bool> filter) =>
            await _schedulerDbContext.Users.Where(u => filter(u)).FirstOrDefaultAsync();
        
        public async Task<User> CreateAsync(User user)
        {
            var createdUser = await _schedulerDbContext.Users.AddAsync(user); 

            return createdUser.Entity;
        }

        public User Delete(User user)
        {
            var deletedUser = _schedulerDbContext.Users.Remove(user);  

            return deletedUser.Entity;
        }

        public async Task<bool> SaveChangesAsync() => 
            await _schedulerDbContext.SaveChangesAsync() > 0; 
    }
}
