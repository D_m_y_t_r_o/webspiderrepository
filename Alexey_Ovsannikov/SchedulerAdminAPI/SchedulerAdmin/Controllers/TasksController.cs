﻿using System;
using System.Collections.Generic;
using System.Linq; 
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc; 
using SchedulerAdmin.Repositories.Interfaces;
using SchedulerAdmin.Models;
using SchedulerAdmin.Dtos;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims; 

namespace SchedulerAdmin.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper; 
          
        public TasksController(ITaskRepository taskRepository, 
                            IUserRepository userRepository, 
                            IMapper mapper)
        {
            _taskRepository = taskRepository;
            _userRepository = userRepository;
            _mapper = mapper; 
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<TaskItemDto>> GetTask(int id)
        {
            var taskItem = await _taskRepository.GetByIdAsync(id);

            if (taskItem == null)
            {
                return NotFound("Task not found in DB.");
            }

            return Ok(_mapper.Map<TaskItemDto>(taskItem));
        }

        /// <summary>
        /// Return list of tasks for specific user
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [HttpGet("user")]
        public async Task<ActionResult<IEnumerable<TaskItemDto>>> GetTasks(string username)
        { 
            IEnumerable<TaskItem> userTasks = await _taskRepository.GetTasksByUserName(username);

            IEnumerable<TaskItemDto> userTasksDto = _mapper.Map<IEnumerable<TaskItem>, IEnumerable<TaskItemDto>>(userTasks);

            return Ok(userTasksDto);
        } 

        // POST api/tasks
        [HttpPost]
        public async Task<ActionResult<TaskItemDto>> PostTask(TaskItemDto taskItemDto)
        {
            TaskItem taskItemToAdd = _mapper.Map<TaskItem>(taskItemDto); 
            taskItemToAdd.User = await _userRepository.GetByFilterAsync(u => u.Username == taskItemDto.Username);

            await _taskRepository.CreateAsync(taskItemToAdd);

            bool success = await _taskRepository.SaveChangesAsync();

            if (!success)
                throw new Exception("Adding item failed on save");

            TaskItem taskItem = await _taskRepository.GetByIdAsync(taskItemToAdd.Id);

            return CreatedAtAction(nameof(GetTask), new { id = taskItem.Id },
                            _mapper.Map<TaskItemDto>(taskItem));
        }

        // PUT api/tasks/1
        [HttpPut("{id}")]
        public async Task<ActionResult<TaskItemDto>> PutTask(int id, TaskItemDto taskItemDto) 
        {
            TaskItem taskToUpdate = _mapper.Map<TaskItem>(taskItemDto);

            bool taskExist = await _taskRepository.GetByIdAsync(id) != null; 

            if (taskExist) 
                return NotFound("Task not found in DB."); 

            _taskRepository.Update(id, taskToUpdate);
            bool success = await _taskRepository.SaveChangesAsync();

            if (!success) 
                throw new Exception("No updates have been written to the database"); 

            return CreatedAtAction(nameof(GetTask), new { id = taskToUpdate.Id },
                            _mapper.Map<TaskItemDto>(taskToUpdate));
        }

        // DELETE api/tasks/1
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTask(int id)
        {
            var taskItem =  await _taskRepository.GetByIdAsync(id); 

            if (taskItem == null) 
                return NotFound("Task not found in DB.");  
            
            _taskRepository.Delete(taskItem);
            bool success = await _taskRepository.SaveChangesAsync();

            if (!success) 
                throw new Exception("Deleting item failed on save"); 

            return Ok(taskItem);
        }
    }
}
