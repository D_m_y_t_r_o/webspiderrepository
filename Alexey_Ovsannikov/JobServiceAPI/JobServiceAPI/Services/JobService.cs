﻿using Experimental.System.Messaging;
using JobServiceAPI.Models;
using JobServiceAPI.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System; 
using System.Net.Http;
using System.Threading.Tasks;

namespace JobServiceAPI.Services
{
    public class JobService : IJobService
    {
        private readonly ILogger<JobService> _logger;
        private string _webSpiderUrl = "https://localhost:44302/api/spider";

        public JobService(ILogger<JobService> logger)
        {
            _logger = logger;
        }

        public TaskData GetNewJobFromQueue()
        {
            TaskData taskData = null;
            using (var queue = new MessageQueue(@".\private$\tasksQ"))
            {
                queue.Formatter = new XmlMessageFormatter(new Type[] { typeof(string) });
                  
                try
                {
                    // Receive and format the message. 
                    string myMessage = queue.Receive(TimeSpan.FromSeconds(1)).Body.ToString();
                    taskData = JsonConvert.DeserializeObject<TaskData>(myMessage);
                }

                catch (MessageQueueException)
                {
                    _logger.LogError("Didn't find any task in queue exception");
                    return null;
                }
                 
                return taskData;
            }
        }
        public async Task<bool> SendJobToWebSpiderAsync(TaskData taskData)
        {
            using (var webSpiderClient = new HttpClient())
            {
                HttpResponseMessage response = await webSpiderClient.PostAsJsonAsync(_webSpiderUrl, taskData);

                return response.IsSuccessStatusCode;
            } 
        }
    }
}
