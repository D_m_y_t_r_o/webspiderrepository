﻿using JobServiceAPI.Models; 
using System.Threading.Tasks;

namespace JobServiceAPI.Services.Interfaces
{
    public interface IJobService
    {
        TaskData GetNewJobFromQueue();
        Task<bool> SendJobToWebSpiderAsync(TaskData taskData); 
    }
}
