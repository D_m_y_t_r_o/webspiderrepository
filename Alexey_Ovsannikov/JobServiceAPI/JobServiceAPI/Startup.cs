﻿using System; 
using JobServiceAPI.Models;
using JobServiceAPI.Models.Interfaces;
using JobServiceAPI.Repositories;
using JobServiceAPI.Services;
using JobServiceAPI.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace JobServiceAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ContentStoreDatabaseSettings>(
                Configuration.GetSection(nameof(ContentStoreDatabaseSettings)));

            services.AddSingleton<IContentStoreDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<ContentStoreDatabaseSettings>>().Value);

            services.AddScoped<IContentRepository, ContentRepository>();
            services.AddScoped<IJobService, JobService>();

            services.AddMvc()
                .AddJsonOptions(options => options.UseMemberCasing())
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new Info { Title = "Swagger Core API", Description = "API for save collected content & get jobs for spider" });

                var xmlPath = System.AppDomain.CurrentDomain.BaseDirectory + @"JobServiceAPI.xml";
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Job service API");
            });
        }
    }
}
