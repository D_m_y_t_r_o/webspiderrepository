﻿using System.Collections.Generic;
using System.Threading.Tasks;
using JobServiceAPI.Models;
using JobServiceAPI.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace JobServiceAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContentController : ControllerBase
    {
        private readonly IContentRepository _contentRepository;

        public ContentController(IContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
        }

        [HttpGet]
        public async Task<ActionResult<List<ContentData>>> GetAllAsync() =>
            await _contentRepository.GetAllAsync();

        [HttpGet("{id:length(24)}", Name = "GetContentData")]
        public async Task<ActionResult<ContentData>> GetAsync(string id)
        {
            ContentData contentData = await _contentRepository.GetAsync(id);

            if (contentData == null) 
                return BadRequest("Task not found."); 

            return contentData;
        }

        [HttpPost]
        public async Task<ActionResult<ContentData>> CreateAsync(ContentData contentData)
        {
            ContentData addedContentData = await _contentRepository.CreateAsync(contentData);

            if (addedContentData == null) 
                return BadRequest(); 

            return CreatedAtRoute("GetContentData", new { id = addedContentData.Id.ToString() }, addedContentData);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> UpdateAsync(string id, ContentData updatedContentData)
        {
            ContentData contentData = await _contentRepository.GetAsync(id);

            if (contentData == null) 
                return NotFound(); 

            await _contentRepository.UpdateAsync(id, updatedContentData);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            var contentData = await _contentRepository.GetAsync(id);

            if (contentData == null) 
                return NotFound(); 

            await _contentRepository.RemoveAsync(contentData.Id);

            return NoContent();
        }
    }
}