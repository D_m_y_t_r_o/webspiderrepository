﻿using System.Threading.Tasks;
using JobServiceAPI.Models;
using JobServiceAPI.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace JobServiceAPI.Controllers
{ 
    [Route("api/[controller]")]
    [ApiController]
    public class JobsController : ControllerBase
    {
        private readonly IJobService _jobService;
        public JobsController(IJobService jobService)
        {
            _jobService = jobService;
        }

        /// <summary>
        /// Checks jobs for execution. If find ready job - sends her to Spider (called from spider)
        /// </summary>
<<<<<<< HEAD
=======
        /// <response code="500">If ready tasks not exists</response>
        /// <response code="400">Faild to send task to WebSpider</response>
>>>>>>> dev_alexeyOvs
        /// <returns></returns>
        [HttpPost("Check")]
        public async Task<ActionResult> SendSpiderNewJob()
        {
            TaskData task = _jobService.GetNewJobFromQueue();

            if (task == null)
            {
                return StatusCode(500);
            }

            bool success = await _jobService.SendJobToWebSpiderAsync(task);

            if (!success)
            {
                return BadRequest("Faild to send task to WebSpider.");
            }

            return Ok();
        }
    }
}