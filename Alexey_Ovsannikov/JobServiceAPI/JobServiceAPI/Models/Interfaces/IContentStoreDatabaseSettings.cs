﻿namespace JobServiceAPI.Models.Interfaces
{ 
    public interface IContentStoreDatabaseSettings
    {
        string ConnectionString { get; set; }
        string ContentDataCollectionName { get; set; }
        string DatabaseName { get; set; }
    }
}
