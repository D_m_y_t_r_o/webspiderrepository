﻿ using System.ComponentModel.DataAnnotations; 

namespace JobServiceAPI.Models
{
    public class TaskData
    { 
        [Required] 
        public string TargetSite { get; set; }
        [Required] 
        public string XPath { get; set; } 
    }
}
