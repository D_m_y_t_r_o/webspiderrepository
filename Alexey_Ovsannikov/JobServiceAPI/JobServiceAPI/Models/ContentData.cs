﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json; 

namespace JobServiceAPI.Models
{
    public class ContentData
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Site")]
        [JsonProperty("Site")]
        public string TargetSite { get; set; }

        [BsonElement("XPath")]
        public string XPath { get; set; }

        [BsonElement("Description")]
        [JsonProperty("Desc")]
        public string Description { get; set; }

        [BsonElement("Data")] 
        public string Data { get; set; }

        [BsonElement("TimeCreation")] 
        [BsonDateTimeOptions(Kind = System.DateTimeKind.Local)]
        public System.DateTime TimeCreation { get; set; }
    }
}
