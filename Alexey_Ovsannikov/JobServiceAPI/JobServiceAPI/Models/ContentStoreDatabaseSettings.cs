﻿using JobServiceAPI.Models.Interfaces; 

namespace JobServiceAPI.Models
{
    public class ContentStoreDatabaseSettings : IContentStoreDatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string ContentDataCollectionName { get; set; }
        public string DatabaseName { get; set; }
    }
}
