﻿using JobServiceAPI.Models;
using JobServiceAPI.Models.Interfaces;
using JobServiceAPI.Services.Interfaces;
using MongoDB.Driver; 
using System.Collections.Generic; 
using System.Threading.Tasks;

namespace JobServiceAPI.Repositories
{ 
    public class ContentRepository : IContentRepository
    {
        private readonly IMongoCollection<ContentData> _contentDataCollection;

        public ContentRepository(IContentStoreDatabaseSettings settings)
        {
            IMongoClient client = new MongoClient(settings.ConnectionString);
            IMongoDatabase database = client.GetDatabase(settings.DatabaseName);

            _contentDataCollection = database.GetCollection<ContentData>(settings.ContentDataCollectionName);
        }

        public async Task<List<ContentData>> GetAllAsync() =>
            await _contentDataCollection.Find(contentData => true).ToListAsync();

        public async Task<ContentData> GetAsync(string id) =>
            await _contentDataCollection.Find<ContentData>(contentData => contentData.Id == id).FirstOrDefaultAsync();

        public async Task<ContentData> CreateAsync(ContentData contentData)
        {
            try
            { 
                await _contentDataCollection.InsertOneAsync(contentData);
            }
            catch
            {
                return null;
            }
            return contentData;
        }

        public async Task UpdateAsync(string id, ContentData contentDataNew) =>
            await _contentDataCollection.ReplaceOneAsync(contentData => contentData.Id == id, contentDataNew);

        public async Task RemoveAsync(ContentData contentDataToDelete) =>
            await _contentDataCollection.DeleteOneAsync(contentData => contentData.Id == contentDataToDelete.Id);

        public async Task RemoveAsync(string id) =>
            await _contentDataCollection.DeleteOneAsync(contentData => contentData.Id == id);
    }
}
