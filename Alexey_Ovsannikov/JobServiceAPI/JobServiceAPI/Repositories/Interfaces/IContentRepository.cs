﻿using JobServiceAPI.Models; 
using System.Collections.Generic; 
using System.Threading.Tasks;

namespace JobServiceAPI.Repositories
{
    public interface IContentRepository
    {
        Task<List<ContentData>> GetAllAsync();
        Task<ContentData> GetAsync(string id);
        Task<ContentData> CreateAsync(ContentData contentData);
        Task UpdateAsync(string id, ContentData contentData);
        Task RemoveAsync(ContentData contentDataToDelete);
        Task RemoveAsync(string id); 
    }
}
