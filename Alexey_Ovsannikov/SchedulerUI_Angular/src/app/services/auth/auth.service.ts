import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  readonly rootURL = 'https://localhost:44337/api/auth/';

  jwtHelper = new JwtHelperService();
  decodedToken: any;

  @Output() authMode: EventEmitter<any> = new EventEmitter();

  httpOptions = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + localStorage.getItem('token')
    })
  };

  constructor(private http: HttpClient, private router: Router) { }

  login(model: any) {
    return this.http.post(this.rootURL + 'login', model)
      .pipe(
        map((response: any) => {
          const user = response;
          if (user) {
            localStorage.setItem('token', user.token);
            this.decodedToken = this.jwtHelper.decodeToken(user.token);
          }
        })
    );
  }

  register(model: any) {
    return this.http.post(this.rootURL + 'register', model);
  }

  checkUserExists() {
    return this.http.get(this.rootURL, { params: { username: this.decodedToken.unique_name } });
  } 

  onLogout() {
    localStorage.removeItem('token');
    this.router.navigateByUrl('/register');
  }

  getUsername() {
    return this.decodedToken.unique_name;
  }
}
