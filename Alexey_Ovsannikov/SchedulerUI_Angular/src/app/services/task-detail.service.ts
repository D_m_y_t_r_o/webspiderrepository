import { Injectable } from '@angular/core'; 
import { HttpClient, HttpParams } from "@angular/common/http";
import { TaskDetail } from '../shared/models/task-detail.model';
import { AuthService } from './auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class TaskDetailService {
  formData: TaskDetail;
  readonly rootURL = "https://localhost:44337/api/";

  list: TaskDetail[];

  constructor(private http: HttpClient, private authService: AuthService) { }

  postTaskDetail() {
    this.formData.Username = this.authService.getUsername();
    return this.http.post(this.rootURL + 'tasks', this.formData, this.authService.httpOptions);
  }

  putTaskDetail() {
    return this.http.put(this.rootURL + 'tasks/' + this.formData.Id, this.formData, this.authService.httpOptions);
  }

  deleteTaskDetail(id) {
    return this.http.delete(this.rootURL + 'tasks/' + id, this.authService.httpOptions);
  }

  refreshList() {
    const params = new HttpParams().set('username', this.authService.getUsername());
    
    this.http.get(this.rootURL + 'tasks/user', { headers: this.authService.httpOptions.headers , params })
              .toPromise()
              .then(res => this.list = res as TaskDetail[]);
  }
}
