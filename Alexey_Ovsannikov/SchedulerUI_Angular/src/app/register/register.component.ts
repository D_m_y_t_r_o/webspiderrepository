import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../services/auth/auth.service'; 
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
 
  model: any = {};

  constructor(private authService: AuthService,
              private toastr: ToastrService) { }

  ngOnInit() {
  }

  register() { // form?: NgForm
    return this.authService.register(this.model).subscribe(() => {
      this.toastr.success('Registration successful', 'User Added'); 
    }, (error) => {
      this.toastr.warning(error, 'User not registred');
    });
  } 
}
