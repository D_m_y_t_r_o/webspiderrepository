export class TaskDetail {
    Id : number;
    TargetSite : string;
    XPath : string;
    TimeStart : Date;
    ExecuteFrequency : string;
    Username: string;
}
