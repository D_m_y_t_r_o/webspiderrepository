import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  model: any = {};

  constructor(protected authService: AuthService,
              private toastr: ToastrService,
              private router: Router
              ) { }

  ngOnInit() {
  }

  login() {
    this.authService.login(this.model).subscribe(next => {
      this.toastr.success('Logged in successfully', 'User authorized');
      this.router.navigateByUrl('/tasks');
    }, (error) => {
      this.toastr.error('Please register', 'User unauthorized');
    });
  }

  loggedIn() {
    const token = localStorage.getItem('token');
    return !!token;
  }

  logout() {
    this.authService.onLogout();
    this.toastr.info('Logget out', 'Message');
  }
}
