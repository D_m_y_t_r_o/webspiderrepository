import { Component, OnInit } from '@angular/core';
import { TaskDetailService } from 'src/app/services/task-detail.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styles: []
})
export class TaskDetailComponent implements OnInit {

  isDisabled = true;
  triggerSomeEvent() {
    this.isDisabled = !this.isDisabled;
    return;
  }

  constructor(protected service: TaskDetailService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.resetForm();
    }

    this.service.formData = {
      Id: 0,
      TargetSite: null,
      XPath: null,
      TimeStart: null,
      ExecuteFrequency: null,
      Username: null
    };
  }

  onSubmit(form: NgForm) {
    if (this.service.formData.Id === 0) {
      this.insertRecord(form);
    } else {
      this.updateRecord(form);
    }
  }

  insertRecord(form: NgForm) {
    this.service.postTaskDetail().subscribe(
      res => {
        this.resetForm(form);
        this.toastr.success('Submitted successfully', 'Task added');
        this.service.refreshList();
      },
      err => {
        console.log(err);
      }
    )
  }
  updateRecord(form: NgForm) {

    this.service.putTaskDetail().subscribe(
      res => {
        this.resetForm(form);
        this.toastr.info('Submitted successfully', 'Task updated');
        this.service.refreshList();
      },
      err => {
        console.log(err);
      }
    )
  }
}
