﻿namespace WebSpider.Models
{
    public class TaskData
    { 
        public string TargetSite { get; set; } 
        public string XPath { get; set; }  
    }
}
