﻿using Newtonsoft.Json; 

namespace WebSpider.Models
{
    public class ContentData
    {  
        [JsonProperty("Site")]
        public string TargetSite { get; set; }
        public string XPath { get; set; }
        [JsonProperty("Desc")]
        public string Description { get; set; } 
        public string Data { get; set; } 
        public System.DateTime TimeCreation { get; set; }
    }
}
