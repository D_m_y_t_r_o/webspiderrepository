﻿using System.Threading.Tasks;
using WebSpider.Models;

namespace WebSpider.Interfaces
{
    public interface ICrawlerService
    { 
        bool IsValidJobOptions(TaskData taskData);
        Task<ContentData> CrawlPage(TaskData taskData);
        Task<bool> SendCollectedContent(ContentData contentData); 

    }
}
