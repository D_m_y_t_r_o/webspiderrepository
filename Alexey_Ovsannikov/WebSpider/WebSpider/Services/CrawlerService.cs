﻿using HtmlAgilityPack; 
using System; 
using System.Net.Http;
using System.Text; 
using System.Threading.Tasks;
using System.Xml.XPath;
using WebSpider.Interfaces;
using WebSpider.Models;

namespace WebSpider.Services
{
    public class CrawlerService : ICrawlerService
    { 
        private readonly string _jobServiceUrl_Content = "https://localhost:44344/api/content";
          
        public bool IsValidJobOptions(TaskData taskData)
        { 
            bool correctUrl = Uri.TryCreate(taskData.TargetSite, UriKind.Absolute, out Uri uriResult)
                                         && uriResult.Scheme == Uri.UriSchemeHttps; 

            bool correctXPath = XPathExpression.Compile(taskData.XPath) != null;

            return correctUrl && correctXPath;
        }

        public async Task<ContentData> CrawlPage(TaskData taskData)
        {
            HtmlDocument htmlPage = await GetPageHtmlText(taskData.TargetSite);

            string targetContent = GetPageContentByXPath(htmlPage, taskData.XPath);

            ContentData contentData = new ContentData()
            {
                TargetSite = taskData.TargetSite,
                XPath = taskData.XPath,
                Data = targetContent,
                Description = "Content from site by XPath", 
                TimeCreation = DateTime.UtcNow
            }; 
             
            return contentData;
        }

        public async Task<bool> SendCollectedContent(ContentData contentData)
        {
            HttpResponseMessage jobServiceResponse = null;
            using (var httpClient = new HttpClient())
            {
                jobServiceResponse = await httpClient.PostAsJsonAsync(_jobServiceUrl_Content, contentData);

                return jobServiceResponse.IsSuccessStatusCode;
            } 
        } 
        private async Task<HtmlDocument> GetPageHtmlText(string url) => 
             await new HtmlWeb().LoadFromWebAsync(url); 

        private string GetPageContentByXPath(HtmlDocument htmlPage, string XPath)
        { 
            var contentNodes = htmlPage.DocumentNode.SelectNodes(XPath);

            StringBuilder resultContent = new StringBuilder(); 
            foreach (HtmlNode node in contentNodes)
            {
                resultContent.Append(node.InnerText);
                resultContent.Append(' ');
            }

            return resultContent.ToString();
        } 
    }
}



//private readonly string _jobServiceUrl_Jobs = "https://localhost:44344/api/jobs";
//public async Task<bool> GetNewTaskItem()
//{
//    HttpResponseMessage jobServiceResponse = null;
//    using (var httpClient = new HttpClient())
//    {
//        jobServiceResponse = await httpClient.PostAsync(_jobServiceUrl_Jobs, null);
//    }

//    if (!jobServiceResponse.IsSuccessStatusCode)
//    {
//        return false;
//    }

//    return true;
//}