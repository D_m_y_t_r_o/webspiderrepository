﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System; 
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace WebSpider.Services
{
    public class ReceivingTasksService : IHostedService, IDisposable
    { 
        private readonly ILogger<ReceivingTasksService> _logger;
        private readonly string _jobServiceUrl_Jobs = "https://localhost:44344/api/jobs/check";

        private Timer _timer;
        private int _executionFrequency = 20;//sec
        private int _executionCount = 0;

        public ReceivingTasksService(ILogger<ReceivingTasksService> logger)
        {
            _logger = logger;
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Timed Hosted Service running.");

            _timer = new Timer(DoWork,
                            null,
                            TimeSpan.Zero,
                            TimeSpan.FromSeconds(_executionFrequency));

            return Task.CompletedTask;
        }

        private async void DoWork(object state)
        { 
            await CheckNewTaskForSpider();
        }

        private async Task CheckNewTaskForSpider()
        { 
            using (var httpClient = new HttpClient())
            {
                await httpClient.PostAsync(_jobServiceUrl_Jobs, null);
            }

            _executionCount++;
            _logger.LogInformation(
                "ReceivingTasksService is working. Count: {Count}", _executionCount);

        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("ReceivingTasksService is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
