﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebSpider.Interfaces;
using WebSpider.Models;

namespace WebSpider.Controllers
{  
    [Route("api/[controller]")]
    [ApiController]
    public class SpiderController : ControllerBase
    { 
        private readonly ICrawlerService _crawler;
          
        public SpiderController(ICrawlerService crawler)
        {
            _crawler = crawler; 
        } 
        [HttpGet]
        public string DefaultGet()
        {
            return "Spider is active.";
        }

        [HttpPost] 
        public async Task<ActionResult> ExecuteTask(TaskData taskData)
        {
            bool isValid = _crawler.IsValidJobOptions(taskData);

            if (!isValid)
            {
                return BadRequest("Invalid site url.");
            }

            ContentData contentData = await _crawler.CrawlPage(taskData);
            
            bool success = await _crawler.SendCollectedContent(contentData);
             
            if (!success)
            {
                return BadRequest("Failed to save. Such content already exists.");
            }
             
            return Ok();
        }
    }
}



//[HttpPost]
//[Route("GetTask")]
//public async Task CheckGetTask() // test
//{ 
//    await _crawler.GetNewTaskItem();
//}