﻿using Experimental.System.Messaging;
using Newtonsoft.Json;
using SchedulerWinService.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace SchedulerWinService
{
    public class WebSpiderTaskChecker
    {
        private readonly Timer _timer;

        public WebSpiderTaskChecker()
        {
            _timer = new Timer(5000) { AutoReset = true }; 
            _timer.Elapsed += TimerElapsed;
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void Start()
        {
            _timer.Start();
        }

        private async void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            await AddReadyTaskToQueue();
        }
        private async Task AddReadyTaskToQueue()
        {
            List<TaskItems> taskItems = await FindTaskReadyForExecutionAsync();

            if (taskItems != null)
            {
                AddTasksToQueue(taskItems);
            }
        }

        public async Task<List<TaskItems>> FindTaskReadyForExecutionAsync()
        {
            List<TaskItems> tasksToExecute = null;

            using (var _schedulerContext = new SchedulerContext())
            {
                tasksToExecute = await _schedulerContext.TaskItems.AsNoTracking().Where(t => t.TimeStart <= DateTime.Now).ToListAsync();

                if (tasksToExecute.Count > 0)
                {
                    #region Update reusable tasks

                    List<TaskItems> reusableTasks = tasksToExecute.FindAll(x => x.ExecuteFrequency != null);
                    foreach (var task in reusableTasks)
                    {
                        _schedulerContext.TaskItems.Attach(task);
                        task.TimeStart = DateTime.Now + task.ExecuteFrequency.GetValueOrDefault(); 
                    }

                    #endregion

                    #region Remove one-time tasks

                    List<TaskItems> oneTimeTasks = await _schedulerContext.TaskItems.Where(x => x.ExecuteFrequency == null).ToListAsync();
                    _schedulerContext.TaskItems.RemoveRange(oneTimeTasks);

                    #endregion

                    await _schedulerContext.SaveChangesAsync();
                }
                else
                { 
                    return null;
                }
            }
             
            return tasksToExecute;
        }

        private void AddTasksToQueue(List<TaskItems> taskItems)
        {
            using (var queue = new MessageQueue())
            {
                queue.Path = @".\private$\tasksQ";
                if (!MessageQueue.Exists(queue.Path))
                {
                    MessageQueue.Create(queue.Path);
                }

                foreach (var task in taskItems)
                {
                    Message msgObj = new Message();
                    msgObj.Formatter = new XmlMessageFormatter(new Type[] { typeof(string) });

                    msgObj.Body = JsonConvert.SerializeObject(
                                                new TaskData() {
                                                    TargetSite = task.TargetSite,
                                                    XPath = task.XPath
                                                });
                    queue.Send(msgObj);
                }
            }
        } 
    } 
}
