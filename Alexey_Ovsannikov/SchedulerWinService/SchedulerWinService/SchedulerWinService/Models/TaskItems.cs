namespace SchedulerWinService
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TaskItems
    {
        public int Id { get; set; }

        [Required]
        [StringLength(150)]
        public string TargetSite { get; set; }

        [Required]
        [StringLength(100)]
        public string XPath { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime TimeStart { get; set; }

        [Description("Time in seconds")]
        public TimeSpan? ExecuteFrequency { get; set; }
    }
}
