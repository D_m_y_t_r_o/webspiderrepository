namespace SchedulerWinService
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SchedulerContext : DbContext
    {
        public SchedulerContext()
            : base("name=SchedulerContext")
        {
        }
         
        public virtual DbSet<TaskItems> TaskItems { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
