﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulerWinService.Models
{
    class TaskData
    {
        [Required]
        public string TargetSite { get; set; }
        [Required]
        public string XPath { get; set; }
    }
}
