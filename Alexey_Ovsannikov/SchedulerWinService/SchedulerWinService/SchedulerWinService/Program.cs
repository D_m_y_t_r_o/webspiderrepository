﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace SchedulerWinService
{
    class Program
    {
        static void Main(string[] args)
        { 
            TopshelfExitCode exitCode = HostFactory.Run(x =>
            {
                x.Service<WebSpiderTaskChecker>(c =>
                {
                    c.ConstructUsing(taskChecker => new WebSpiderTaskChecker());
                    c.WhenStarted(taskChecker => taskChecker.Start());
                    c.WhenStopped(taskChecker => taskChecker.Stop());
                });

                x.RunAsLocalSystem();

                x.SetServiceName("WebSpiderTaskCheckerService");
                x.SetDisplayName("WebSpider TasksChecker Service");
                x.SetDescription("This service checking sql database on ready tasks for WebSpider.");
            });

            int exitCodeValue = (int)Convert.ChangeType(exitCode, exitCode.GetTypeCode());
            Environment.ExitCode = exitCodeValue;
        }
    }
}
