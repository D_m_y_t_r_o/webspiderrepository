﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestAPI.Services;
using TestAPI.Models;
namespace TestAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PagesController:ControllerBase
    {  
        private readonly PageService _pageService;
        public PagesController(PageService pageService)
        {
            _pageService = pageService;
        }

        [HttpGet]
        public ActionResult<List<Page>> Get() =>
            _pageService.Get();

        [HttpGet("{id:length(24)}", Name = "Getpage")]
        public ActionResult<Page> Get(string id)
        {
            var page = _pageService.Get(id);

            if (page == null)
            {
                return NotFound();
            }

            return page;
        }

        [HttpPost]
        public ActionResult<Page> Create(Page page)
        {
            _pageService.Create(page);

            return CreatedAtRoute("Getpage", new { id = page.Id.ToString() }, page);
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, Page pageIn)
        {
            var page = _pageService.Get(id);

            if (page == null)
            {
                return NotFound();
            }

            _pageService.Update(id, pageIn);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var page = _pageService.Get(id);

            if (page == null)
            {
                return NotFound();
            }

            _pageService.Remove(page.Id);

            return NoContent();
        }
    }
}
