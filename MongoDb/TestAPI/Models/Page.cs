﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace TestAPI.Models
{
    public class Page
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        
        [BsonElement("URL")]
        public string Url { get; set; }

        [BsonElement("TagInfo")]
        public string TagInfo { get; set; }
        [BsonElement("TagResult")]
        public string TagResult { get; set; }
       
        [BsonDateTimeOptions]
        public DateTime TimeStart { get; set; }
    }
}
