﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestAPI.Models
{
    public class PagesDatabaseSettings : IPagesDatabaseSettings
    {
        public string PagesCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public  string DatabaseName { get; set; }
    }
}
