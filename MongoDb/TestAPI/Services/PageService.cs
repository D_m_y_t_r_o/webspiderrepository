﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestAPI.Models;
using MongoDB.Driver;
using TestAPI.Services;

namespace TestAPI.Services
{
    public class PageService
    {
        private readonly IMongoCollection<Page> _pages;
        public PageService(IPagesDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _pages = database.GetCollection<Page>(settings.PagesCollectionName);
        }
        public List<Page> Get() =>
             _pages.Find(page => true).ToList();
        public Page Get(string id) =>
            _pages.Find(page => page.Id == id).FirstOrDefault();
        public Page Create(Page page)
        {
            _pages.InsertOne(page);
            return page;

        }
        public void Update(string id,  Page pageIn) =>
             _pages.ReplaceOne(page => page.Id == id, pageIn);
        public void Remove(Page pageIn) =>
           _pages.DeleteOne(page => page.Id == pageIn.Id);

        public void Remove(string id) =>
            _pages.DeleteOne(page => page.Id == id);

    }
       
}
