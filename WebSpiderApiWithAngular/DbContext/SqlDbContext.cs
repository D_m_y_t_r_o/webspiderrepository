﻿using Microsoft.EntityFrameworkCore;
using WebSpiderApiWithAngular.Models;

namespace WebSpiderApiWithAngular
{
    public class SqlDbContext : DbContext
    {
        public SqlDbContext(DbContextOptions<SqlDbContext> options) : base(options)
        {
        }

        public DbSet<TaskItem> TaskItems { get; set; }
    }
}
