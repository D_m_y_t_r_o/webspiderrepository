﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using WebSpiderApiWithAngular.Extentions;
using WebSpiderApiWithAngular.Models;

namespace WebSpiderApiWithAngular.SpiderService
{
    public class WebSpiderHandler : ISpider
    {
        private readonly HtmlWeb web;
        public WebSpiderHandler()
        {
            web = new HtmlWeb();
        }
        public HtmlNodeCollection GetNodesFromSite(TaskItem siteInfo)
        {
            var isValid = Uri.IsWellFormedUriString(siteInfo.TargetSite, UriKind.Absolute);

            if (isValid)
            {
                try
                {
                    return web
                        .Load(siteInfo.TargetSite)?
                        .DocumentNode?
                        .SelectNodes(siteInfo.XPath);
                }
                catch
                {
                    return null;
                }
            }

            return null;
        }

        public string GetTextFromSite(TaskItem siteInfo) =>
            GetNodesFromSite(siteInfo)?
            .Select(node => node.InnerText.Trim())
            .ConcatAllElements();

        public IEnumerable<ResultItem> GetResultList(IEnumerable<TaskItem> items) =>
            items.Select(x => new ResultItem
            {
                BodyText = GetTextFromSite(x),
                Time = DateTime.Now,
                Url = x.TargetSite,
                XPath = x.XPath
            })
            .Where(x => !string.IsNullOrEmpty(x.BodyText));

    }
}
