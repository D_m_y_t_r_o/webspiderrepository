﻿using HtmlAgilityPack;
using System.Collections.Generic;
using WebSpiderApiWithAngular.Models;

namespace WebSpiderApiWithAngular.SpiderService
{
    public interface ISpider
    {
        HtmlNodeCollection GetNodesFromSite(TaskItem siteInfo);
        string GetTextFromSite(TaskItem siteInfo);
        IEnumerable<ResultItem> GetResultList(IEnumerable<TaskItem> items);
    }
}
