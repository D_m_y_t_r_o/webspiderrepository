﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebSpiderApiWithAngular.IDbRepository;
using WebSpiderApiWithAngular.Models;

namespace WebSpiderApiWithAngular.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResultController : Controller
    {
        private readonly IMongoDbRepository repository;

        public ResultController(IMongoDbRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        public async Task<IEnumerable<ResultItem>> GetAllResults() =>
            await repository.GetAllResultsAsync();

        [HttpGet("{id:length(24)}", Name = "GetResult")]
        public async Task<ActionResult<ResultItem>> GetResult(string id)
        {
            var result = await repository.GetResultAsync(id);

            if (result == null)
            {
                return NotFound();
            }

            return result;
        }

        [HttpPost]
        public async Task<ActionResult<ResultItem>> Create(ResultItem item)
        {
            var result = await repository.CreateAsync(item);

            return CreatedAtRoute(nameof(GetResult), new { id = item.Id }, item);
        }

        [HttpPost("{id:length(24)}")]
        public async Task<IActionResult> Update(string id, ResultItem item)
        {
            var result = await repository.GetResultAsync(id);

            if (result == null)
            {
                return NotFound();
            }

            await repository.UpdateAsync(id, item);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await repository.GetResultAsync(id);

            if (result == null)
            {
                return NotFound();
            }

            await repository.RemoveAsync(id);

            return NoContent();
        }
    }
}