﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebSpiderApiWithAngular.IDbRepository;
using WebSpiderApiWithAngular.Models;

namespace SchedulerAdmin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskRepository taskRepository;

        public TaskController(ITaskRepository taskRepository)
        {
            this.taskRepository = taskRepository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskItem>>> GetTasks()
        {
            return Ok(await taskRepository.GetAllAsync());
        }

        // GET api/tasks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TaskItem>> GetTask(int id)
        {
            var taskItem = await taskRepository
                .GetByIdAsync(id);

            if (taskItem == null)
            {
                return NotFound();
            }

            return taskItem;
        }

        [HttpGet("GetSchedulerTasks")]
        public IEnumerable<TaskItem> GetSchedulerTasks()
        {
            Func<TaskItem, bool> myBoolPredicate = item =>
            {
                var timeInMinute = DateTime.Now.Hour * 60 + DateTime.Now.Minute;
                var time = ((double)timeInMinute - item.TimeStart.Hour * 60 + item.TimeStart.Minute) % item.ExecuteFrequency.Minutes;

                return (item.TimeStart <= DateTime.Now) &&
                            (time >= 0) && (time <= 1);
            };

            return taskRepository
                .GetTaskItemsWithouTracking
                .Where(myBoolPredicate)
                .ToList();
        }

        // POST api/tasks
        [HttpPost]
        public async Task<ActionResult<TaskItem>> PostTask(TaskItem taskItem)
        {
            if (!ModelState.IsValid)
                return BadRequest("Validation problem");

            await taskRepository
                .AddAsync(taskItem);

            bool success = await taskRepository.SaveAsync();
            if (!success)
            {
                BadRequest("Adding item failed on save");
            }

            return CreatedAtAction(nameof(GetTask), new { id = taskItem.Id }, taskItem);
        }

        // PUT api/tasks/1
        [HttpPut("{id}")]
        public async Task<ActionResult<TaskItem>> PutTask(int id, TaskItem item)
        {
            if (!ModelState.IsValid)
                return BadRequest("Validation problem");

            if (id != item.Id)
                return BadRequest("Invalid ID");

            var updatedTaskItem = taskRepository.Update(id, item);
            if (updatedTaskItem == null)
            {
                return NotFound();
            }

            bool success = await taskRepository.SaveAsync();
            if (!success)
            {
                BadRequest("No updates have been written to the database");
            }

            return CreatedAtAction(nameof(GetTask), new { id = updatedTaskItem.Id }, updatedTaskItem);
        }

        // DELETE api/tasks/1
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTask(int id)
        {
            var taskResult = taskRepository.Delete(id);
            if (taskResult == null)
            {
                return NotFound();
            }

            bool success = await taskRepository.SaveAsync();
            if (!success)
            {
                BadRequest("Deleting item failed on save");
            }

            return Ok();
        }
    }
}
