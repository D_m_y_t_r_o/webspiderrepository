﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebSpiderApiWithAngular.IDbRepository;
using WebSpiderApiWithAngular.Models;
using WebSpiderApiWithAngular.SpiderService;

namespace WebSpiderApiWithAngular.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobServiceController : Controller
    {
        private readonly IMongoDbRepository repository;
        private readonly ISpider spider;
        public JobServiceController(IMongoDbRepository repository, ISpider spider)
        {
            this.repository = repository;
            this.spider = spider;
        }

        [HttpPost]
        public async Task<IActionResult> StartJobFromQeue(IEnumerable<TaskItem> items)
        {
            IEnumerable<ResultItem> resultList = spider.GetResultList(items);

            if (resultList.Any())
            {
                await repository.CreateManyAsync(resultList);
            }

            return Ok();
        }
    }
}