﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using WebSpiderApiWithAngular.DbRepository;
using WebSpiderApiWithAngular.IDbRepository;
using WebSpiderApiWithAngular.SpiderService;

namespace WebSpiderApiWithAngular
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            //                     Warning!                 //
            //                                              //
            //  Add Connection String(SQL) to appsettings ! //

            var connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContextPool<SqlDbContext>(x => x.UseSqlServer(connection));
            //  Write TaskDbRepository instead Fake // 
            services.AddScoped<ITaskRepository, TaskDbRepository>();


            //                     Warning!                 //
            //                                              //
            //  Add Connection String(MongoDb) to appsettings!
      
            services.Configure<MongoDbBaseSettings>(
                    Configuration.GetSection(nameof(MongoDbBaseSettings)));

            services.AddSingleton<IMongoDataBaseSettings>(sp =>
                sp.GetRequiredService<IOptions<MongoDbBaseSettings>>().Value);
            
            // Write MongoDbRepository instead Fake
            services.AddSingleton<IMongoDbRepository, MongoDbRepository>();

            services.AddScoped<ISpider, WebSpiderHandler>();

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options => {
                    var resolver = options.SerializerSettings.ContractResolver;
                    if (resolver != null)
                        (resolver as DefaultContractResolver).NamingStrategy = null;
                });

            services.AddCors();

            // Scheduler
            //services.AddHostedService<TimedHostedService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(option => 
            option.WithOrigins("http://localhost:4200")
            .AllowAnyMethod()
            .AllowAnyHeader());

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
