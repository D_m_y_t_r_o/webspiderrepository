﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using WebSpiderApiWithAngular.Models;

namespace WebSpiderApiWithAngular
{
    public class TimedHostedService : IHostedService, IDisposable
    {
        private readonly ILogger logger;
        private Timer timer;
        public TimedHostedService(ILogger<TimedHostedService> logger)
        {
            this.logger = logger;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("Service has been started");

            timer = new Timer(
                async (state) => await Job(state),
                null,
                TimeSpan.Zero,
                TimeSpan.FromMinutes(1)
                );

            return Task.CompletedTask;
        }    

        private async Task Job(object state)
        {
            using (var client = new HttpClient())
            {
                var postTask = await client.GetAsync("https://localhost:5001/api/task/GetSchedulerTasks");

                if (postTask.IsSuccessStatusCode)
                {
                    var readTask = await postTask.Content.ReadAsAsync<IEnumerable<TaskItem>>();

                    if (readTask.Any())
                    {
                        logger.LogInformation(readTask.FirstOrDefault().TargetSite);

                        var response = await client.PostAsJsonAsync("https://localhost:5001/api/JobService", readTask);

                        if (response.IsSuccessStatusCode)
                        {
                            logger.LogInformation("Success");
                        }
                    }
                }
                else
                {
                    logger.LogInformation("Some Error");
                }
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("Timed Background Service is stopping.");

            timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose() =>
            timer?.Dispose();
    }
}
