﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebSpiderApiWithAngular.Models
{
    public class TaskItem
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "You must input site address")]
        [Column(TypeName = "nvarchar(50)")]
        public string TargetSite { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(100)")]
        public string XPath { get; set; }
        [Required]
        public DateTime TimeStart { get; set; }
        [Required]
        public TimeSpan ExecuteFrequency { get; set; }
    }
}