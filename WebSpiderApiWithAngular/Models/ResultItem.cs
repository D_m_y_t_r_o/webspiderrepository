﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebSpiderApiWithAngular.Models
{
   [Table("resultFromSite")]
    public class ResultItem
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRequired]
        public string Url { get; set; } = string.Empty;
        [BsonRequired]
        public string BodyText { get; set; } = string.Empty;
        [BsonRequired]
        public string XPath { get; set; } = string.Empty;
        public DateTime Time { get; set; } = DateTime.Now;
    }
}
