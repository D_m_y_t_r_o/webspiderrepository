﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSpiderApiWithAngular.IDbRepository;
using WebSpiderApiWithAngular.Models;

namespace WebSpiderApiWithAngular.DbRepository
{
    public class MongoDbRepository : IMongoDbRepository
    {
        private readonly IMongoCollection<ResultItem> resultCollection;

        public MongoDbRepository(IMongoDataBaseSettings settings)
        {
            resultCollection = new MongoClient(settings.ConnectionString)
                .GetDatabase(settings.DataBaseName)
                .GetCollection<ResultItem>(settings.CollectionName);
        }
        public async Task<IEnumerable<ResultItem>> GetAllResultsAsync() =>
            await resultCollection.FindAsync(task => true)
            .Result
            .ToListAsync();

        public async Task<ResultItem> GetResultAsync(string id) =>
            await resultCollection.FindAsync(item => item.Id == id)
            .Result
            .FirstOrDefaultAsync();

        public async Task<ResultItem> CreateAsync(ResultItem item)
        {
            await resultCollection.InsertOneAsync(item);
            return item;
        }

        public async Task CreateManyAsync(IEnumerable<ResultItem> items)
        {
            await resultCollection.InsertManyAsync(items);
        }

        public async Task UpdateAsync(string id, ResultItem item) =>
            await resultCollection
            .ReplaceOneAsync(x => x.Id == id, item);

        public async Task RemoveAsync(ResultItem item) =>
            await resultCollection
            .DeleteOneAsync(x => x.Id == item.Id);

        public async Task RemoveAsync(string id) =>
            await resultCollection
            .DeleteOneAsync(item => item.Id == id);
    }
}
