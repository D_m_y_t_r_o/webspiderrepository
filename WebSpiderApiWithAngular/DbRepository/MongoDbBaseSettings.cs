﻿using WebSpiderApiWithAngular.IDbRepository;

namespace WebSpiderApiWithAngular.DbRepository
{
    public class MongoDbBaseSettings : IMongoDataBaseSettings
    {
        public string CollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DataBaseName { get; set; }
    }
}
