﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSpiderApiWithAngular.IDbRepository;
using WebSpiderApiWithAngular.Models;

namespace WebSpiderApiWithAngular.DbRepository
{
    public class TaskDbRepository : ITaskRepository
    {
        private readonly SqlDbContext schedulerContext;

        public TaskDbRepository(SqlDbContext schedulerDbContext)
        {
            schedulerContext = schedulerDbContext;
        }

        public IQueryable<TaskItem> GetTaskItemsWithouTracking =>
            schedulerContext
            .TaskItems
            .AsNoTracking();

        public async Task<IEnumerable<TaskItem>> GetAllAsync() =>
            await GetTaskItemsWithouTracking
                .ToListAsync();

        public TaskItem GetById(int id) =>
            GetTaskItemsWithouTracking
            .FirstOrDefault(x => x.Id == id);

        public async Task<TaskItem> GetByIdAsync(int id) =>
           await GetTaskItemsWithouTracking
            .FirstOrDefaultAsync(x => x.Id == id);

        public async Task AddAsync(TaskItem taskItem) =>
            await schedulerContext
            .TaskItems
            .AddAsync(taskItem);

        public TaskItem Delete(int id)
        {
            var taskItem = this.GetById(id);
            if (taskItem == null)
            {
                return null;
            }
            schedulerContext
                .TaskItems
                .Remove(taskItem);

            return taskItem;
        }

        public TaskItem Delete(TaskItem taskItem)
        {
            if (taskItem == null)
                return null;

            schedulerContext
            .TaskItems
            .Remove(taskItem);

            return taskItem;
        }

        public async Task<int> CountAsync() =>
            await schedulerContext
            .TaskItems
            .AsNoTracking()
            .CountAsync();

        public async Task<bool> SaveAsync() =>
            await schedulerContext.SaveChangesAsync() > 0;

        public TaskItem Update(int id, TaskItem taskItem)
        {
            var taskToUpdate = this.GetById(id);

            if (taskToUpdate == null)
                return null;

            schedulerContext.TaskItems.Update(taskItem);
            return taskItem;
        }
    }
}
