﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSpiderApiWithAngular.IDbRepository;
using WebSpiderApiWithAngular.Models;

namespace WebSpiderApiWithAngular.DbRepository
{
    public class FakeTaskRepository : ITaskRepository, IMongoDbRepository
    {
        public IQueryable<TaskItem> GetTaskItemsWithouTracking => throw new NotImplementedException();

        public Task<IEnumerable<ResultItem>> GetAllResultsAsync()
        {
            throw new NotImplementedException();
        }

        public Task<ResultItem> GetResultAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<ResultItem> CreateAsync(ResultItem book)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(string id, ResultItem bookIn)
        {
            throw new NotImplementedException();
        }

        public Task RemoveAsync(ResultItem bookIn)
        {
            throw new NotImplementedException();
        }

        public Task RemoveAsync(string id)
        {
            throw new NotImplementedException();
        }

        public TaskItem GetById(int id)
        {
            throw new NotImplementedException();
        }

        TaskItem ITaskRepository.Delete(TaskItem id)
        {
            throw new NotImplementedException();
        }

        public TaskItem Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<TaskItem>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public Task<TaskItem> GetByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task AddAsync(TaskItem taskItem)
        {
            throw new NotImplementedException();
        }

        public TaskItem Update(int id, TaskItem taskItem)
        {
            throw new NotImplementedException();
        }

        public Task<int> CountAsync()
        {
            throw new NotImplementedException();
        }

        public Task<bool> SaveAsync()
        {
            throw new NotImplementedException();
        }

        public Task CreateManyAsync(IEnumerable<ResultItem> items)
        {
            throw new NotImplementedException();
        }
    }
}
