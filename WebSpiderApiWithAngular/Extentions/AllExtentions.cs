﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSpiderApiWithAngular.Extentions
{
    public static class AllExtentions
    {
        public static string ConcatAllElements<T>(this IEnumerable<T> InNumerable) =>
            string.Join("\n", (InNumerable ?? new T[] { }) );
    }
}
