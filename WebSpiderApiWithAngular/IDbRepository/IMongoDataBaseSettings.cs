﻿
namespace WebSpiderApiWithAngular.IDbRepository
{
    public interface IMongoDataBaseSettings
    {
        string CollectionName { get; set; }
        string ConnectionString { get; set; }
        string DataBaseName { get; set; }
    }
}
