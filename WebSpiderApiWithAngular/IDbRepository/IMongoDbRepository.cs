﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSpiderApiWithAngular.Models;

namespace WebSpiderApiWithAngular.IDbRepository
{
    public interface IMongoDbRepository
    {
        Task<IEnumerable<ResultItem>> GetAllResultsAsync();
        Task<ResultItem> GetResultAsync(string id);
        Task<ResultItem> CreateAsync(ResultItem item);
        Task CreateManyAsync(IEnumerable<ResultItem> items);
        Task UpdateAsync(string id, ResultItem itemId);
        Task RemoveAsync(ResultItem ItemIn);
        Task RemoveAsync(string id);
    }
}
