﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSpiderApiWithAngular.Models;

namespace WebSpiderApiWithAngular.IDbRepository
{
    public interface ITaskRepository
    {
        IQueryable<TaskItem> GetTaskItemsWithouTracking { get; }
        Task<IEnumerable<TaskItem>> GetAllAsync();
        TaskItem GetById(int id);
        Task<TaskItem> GetByIdAsync(int id);
        Task AddAsync(TaskItem taskItem);
        TaskItem Delete(TaskItem id);
        TaskItem Delete(int id);
        TaskItem Update(int id, TaskItem taskItem);
        Task<int> CountAsync();
        Task<bool> SaveAsync();
    }
}
