﻿using SpiderConsole.JobForScheduler;
using System;
using System.Threading;

namespace SpiderConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            ISpiderModuleJob spiderModuleJob = new SpiderModuleJob();

            var timer = new Timer(
                async (state) => await spiderModuleJob.DoSpiderJob(),
                null,
                TimeSpan.Zero,
                TimeSpan.FromSeconds(15));

            Console.ReadKey();
        }
    }
}
