﻿using HtmlAgilityPack;
using SpiderConsole.Models;
using System.Collections.Generic;

namespace SpiderConsole.SpiderServiceFolder
{
    public interface ISpiderService
    {
        HtmlNodeCollection GetNodesFromSite(PageInfo siteInfo);
        string GetTextFromSite(PageInfo siteInfo);
        IEnumerable<ResultItem> GetResultList(IEnumerable<PageInfo> items);
    }
}
