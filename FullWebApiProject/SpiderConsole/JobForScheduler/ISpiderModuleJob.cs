﻿using System.Threading.Tasks;

namespace SpiderConsole.JobForScheduler
{
    public interface ISpiderModuleJob
    {
        Task DoSpiderJob();
    }
}
