﻿using SpiderConsole.Models;
using SpiderConsole.SpiderFolder;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpiderConsole.JobForScheduler
{
    public class SpiderModuleJob : ISpiderModuleJob
    {
        private readonly ISpider spider;
        private const string getSpiderJob = "https://localhost:5002/api/jobService/GetJob";
        private const string sendSpiderResult = "https://localhost:5002/api/jobService/CreateMany";
        public SpiderModuleJob()
        {
            spider = new Spider();
        }

        public async Task DoSpiderJob()
        {
            if (!(await spider.CheckUrl(getSpiderJob)))
                return;

            var tasksForSpider = await spider.GetJob<IEnumerable<PageInfo>>(getSpiderJob);

            if (tasksForSpider is null)
                return;

            var getResults = spider.InspectPages(tasksForSpider);

            if (getResults is null)
                return;

            var jobSeviceResponse = await spider.SendResult(sendSpiderResult, getResults);
        }

    }
}
