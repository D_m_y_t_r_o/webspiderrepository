﻿
namespace SpiderConsole.Models
{
    public class PageInfo
    {
        public string TargetSite { get; set; }
        public string XPath { get; set; }
    }
}
