﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpiderConsole.Models
{
    public class ResultItem
    {
        public string Id { get; set; }
        public string Url { get; set; }
        public string BodyText { get; set; }
        public string XPath { get; set; }
        public DateTime Time { get; set; }
    }
}
