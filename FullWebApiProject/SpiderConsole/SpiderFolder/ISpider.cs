﻿using SpiderConsole.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpiderConsole.SpiderFolder
{
    public interface ISpider : IDisposable
    {
        Task<bool> CheckUrl(string url);
        Task<string> GetJob(string url);
        Task<TOut> GetJob<TOut>(string url) where TOut : class;
        IEnumerable<ResultItem> InspectPages(IEnumerable<PageInfo> items);
        Task<bool> SendResult(string url, IEnumerable<ResultItem> resultItems);
    }
}
