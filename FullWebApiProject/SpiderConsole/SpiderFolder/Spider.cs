﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpiderConsole.Models;
using SpiderConsole.SpiderServiceFolder;
using HttpClientExtensions;
using System.Net.Http;

namespace SpiderConsole.SpiderFolder
{
    class Spider : ISpider
    {
        private readonly HttpClient client;
        private readonly ISpiderService spiderService;

        public Spider()
        {
            client = new HttpClient();
            spiderService = new SpiderService();
        }

        public async Task<bool> CheckUrl(string url)
        {
            return await client.UriIsAvailable(url);
        }

        public async Task<string> GetJob(string url)
        {
            return await client.GetRequestContent(url);
        }

        public async Task<TOut> GetJob<TOut>(string url) where TOut : class
        {
            return await client.GetRequestContent<TOut>(url);
        }

        public IEnumerable<ResultItem> InspectPages(IEnumerable<PageInfo> items)
        {
            if (items is null || !items.Any())
                return null;

            var result = spiderService.GetResultList(items);

            return result;
        }

        public async Task<bool> SendResult(string url, IEnumerable<ResultItem> resultItems)
        {
            if (resultItems is null || !resultItems.Any())
                return false;

            var success = await client.PostRequest(url, resultItems);

            return success;
        }

        public void Dispose() =>
            client.Dispose();

    }
}
