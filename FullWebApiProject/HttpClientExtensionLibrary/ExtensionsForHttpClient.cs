﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace HttpClientExtensions
{
    public static class ExtensionsForHttpClient
    {
        public static async Task<bool> UriIsAvailable(this HttpClient client, string url)
        {
            try
            {
                if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                    return false;

                var response = await client.GetAsync(url);
                return response.IsSuccessStatusCode;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static async Task<string> GetRequestContent(this HttpClient client, string url)
        {
            var getTasks = await client.GetAsync(url);

            if (!getTasks.IsSuccessStatusCode)
                return null;

            var result = await getTasks.Content.ReadAsStringAsync();

            return result;
        }

        public static async Task<TOut> GetRequestContent<TOut>(this HttpClient client, string url)
            where TOut : class
        {
            var getTasks = await client.GetAsync(url);

            if (!getTasks.IsSuccessStatusCode)
                return null;

            var stringResult = await getTasks.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<TOut>(stringResult);

            return result;
        }

        public static async Task<bool> PostRequest<TIn>(this HttpClient client, string url, TIn value)
        {
            var response = await client.PostAsJsonAsync(url, value);

            return response.IsSuccessStatusCode;
        }

        public static async Task<string> PostRequestWithAnswer<TIn>(this HttpClient client, string url, TIn value)
        {
            var response = await client.PostAsJsonAsync(url, value);

            if (!response.IsSuccessStatusCode)
                return null;

            var result = await response.Content.ReadAsStringAsync();

            return result;
        }

        public static async Task<TOut> PostRequestWithAnswer<TIn, TOut>(this HttpClient client, string url, TIn value)
            where TOut : class
        {
            var response = await client.PostAsJsonAsync(url, value);

            if (!response.IsSuccessStatusCode)
                return null;

            var stringResult = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<TOut>(stringResult);

            return result;
        }
    }
}
