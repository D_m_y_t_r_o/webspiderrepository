﻿using JobServiceAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobServiceAPI.IDbRepository
{
    public interface IMongoDbRepository
    {
        Task<IEnumerable<ResultItem>> GetAllResultsAsync();
        Task<ResultItem> GetResultAsync(string id);
        Task CreateAsync(ResultItem item);
        Task CreateManyAsync(IEnumerable<ResultItem> items);
        Task UpdateAsync(string id, ResultItem itemId);
        Task RemoveAsync(ResultItem ItemIn);
        Task RemoveAsync(string id);
    }
}
