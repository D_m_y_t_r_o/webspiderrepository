﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using JobServiceAPI.IDbRepository;
using JobServiceAPI.Models;
using Microsoft.AspNetCore.Mvc;
using HttpClientExtensions;

namespace JobServiceAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobServiceController : ControllerBase
    {
        private readonly IMongoDbRepository repository;
        private const string getTasksFromQueue = "https://localhost:5005/api/Queue/GetTasks";
        private const string queueApi = "https://localhost:5005/api/Queue";


        public JobServiceController(IMongoDbRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get Ok response
        /// </summary>
        [HttpGet]
        public ActionResult Get() =>
            Ok();

        /// <summary>
        /// Get all results
        /// </summary>
        /// <response code="200">Returns the list of ResultItem</response>
        [HttpGet("getAllResults")]
        public async Task<ActionResult<IEnumerable<ResultItem>>> GetAllResults() =>
            Ok(await repository.GetAllResultsAsync());

        /// <summary>
        /// Get job for spider in string format
        /// </summary>
        /// <response code="200">Returns string</response>
        /// <response code="404">if no job for spider</response>
        [HttpGet("getJob")]
        public async Task<ActionResult<string>> GetJob()
        {
            using (var client = new HttpClient())
            {
                if (!(await client.UriIsAvailable(queueApi)))
                    return NotFound("Queue api is not available");

                var responseFromQueue = await client.GetAsync(getTasksFromQueue);

                if (!responseFromQueue.IsSuccessStatusCode)
                    return NotFound("No Job");

                var tasks = await responseFromQueue.Content.ReadAsStringAsync();

                return Ok(tasks);
            }
        }

        /// <summary>
        /// Get ResultItem
        /// </summary>
        /// <param name="id">id(length:24) of the ResultItem</param>
        /// <response code="200">Returns ResultItem</response>
        /// <response code="400">If ResultItem not found in DataBase</response>
        [HttpGet("{id:length(24)}", Name = "GetResult")]
        public async Task<ActionResult<ResultItem>> GetResult(string id)
        {
            var result = await repository.GetResultAsync(id);

            if (result == null)
                return BadRequest("Not found in DataBase");

            return Ok(result);
        }

        /// <summary>
        /// Create new ResultItem
        /// </summary>
        /// <remarks>
        /// Note that dublicate will not save!
        /// Predicate for dublicate(3 parameters): equals 'Url' and equals 'BodyText' and equals 'XPath'
        /// </remarks>
        /// <param name="item">ResultItem</param>
        /// <response code="201">Returns new created ResultItem</response>
        [HttpPost("Create")]
        public async Task<ActionResult<ResultItem>> Create(ResultItem item)
        {
            await repository.CreateAsync(item);

            return CreatedAtRoute(nameof(GetResult), new { id = item.Id }, item);
        }

        /// <summary>
        /// Create new list of ResultItem
        /// </summary>
        /// <remarks>
        /// Note that dublicates will not save!
        /// Predicate for dublicate(3 parameters): equals 'Url' and equals 'BodyText' and equals 'XPath'
        /// </remarks>
        /// <param name="items">list of ResultItem</param>
        /// <response code="204">Complete</response>
        [HttpPost("CreateMany")]
        public async Task<ActionResult> CreateMany(IEnumerable<ResultItem> items)
        {
            await repository.CreateManyAsync(items);

            return NoContent();
        }

        /// <summary>
        /// Edit ResultItem
        /// </summary>
        /// <param name="id">id(length:24) of the ResultItem</param>
        /// <param name="item">ResultItem model</param>
        /// <response code="204">Complete</response>
        /// <response code="400">If not found in dataBase</response>
        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update(string id, ResultItem item)
        {
            var result = await repository.GetResultAsync(id);

            if (result == null)
                return BadRequest("Not found in DataBase");

            await repository.UpdateAsync(id, item);

            return NoContent();
        }

        /// <summary>
        /// Delete ResultItem
        /// </summary>
        /// <param name="id">id(length:24) of the ResultItem</param>
        /// <response code="204">Complete</response>
        /// <response code="400">If not found in dataBase</response>
        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete(string id)
        {
            var result = await repository.GetResultAsync(id);

            if (result == null)
                return BadRequest("Not found in DataBase");

            await repository.RemoveAsync(id);

            return NoContent();
        }
    }
}
