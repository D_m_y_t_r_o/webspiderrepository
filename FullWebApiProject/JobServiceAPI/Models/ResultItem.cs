﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace JobServiceAPI.Models
{
    [Table("resultFromSite")]
    public class ResultItem
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonRequired]
        public string Url { get; set; } = string.Empty;

        [BsonRequired]
        public string BodyText { get; set; } = string.Empty;

        [BsonRequired]
        public string XPath { get; set; } = string.Empty;

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime Time { get; set; } = DateTime.UtcNow;
    }
}
