﻿using JobServiceAPI.IDbRepository;

namespace JobServiceAPI.DbRepository
{
    public class MongoDbSettings : IMongoDbSettings
    {
        public string CollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DataBaseName { get; set; }
    }
}
