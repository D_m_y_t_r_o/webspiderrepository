﻿using JobServiceAPI.IDbRepository;
using JobServiceAPI.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobServiceAPI.DbRepository
{
    public class MongoDbRepository : IMongoDbRepository
    {
        private readonly IMongoCollection<ResultItem> resultCollection;

        public MongoDbRepository(IMongoDbSettings settings)
        {
            resultCollection = new MongoClient(settings.ConnectionString)
                .GetDatabase(settings.DataBaseName)
                .GetCollection<ResultItem>(settings.CollectionName);
        }

        public async Task<IEnumerable<ResultItem>> GetAllResultsAsync() =>
            await resultCollection.Find(task => true)
            .ToListAsync();

        public async Task<ResultItem> GetResultAsync(string id) =>
            await resultCollection.Find(item => item.Id == id)
            .FirstOrDefaultAsync();

        public async Task CreateAsync(ResultItem item)
        {
            try
            {
                await resultCollection.InsertOneAsync(item);
            }
            catch { }
        }

        public async Task CreateManyAsync(IEnumerable<ResultItem> items)
        {
            try
            {
                await resultCollection.InsertManyAsync(items, new InsertManyOptions { IsOrdered = false });
            }
            catch { }
        }

        public async Task UpdateAsync(string id, ResultItem item) =>
            await resultCollection
            .ReplaceOneAsync(x => x.Id == id, item);

        public async Task RemoveAsync(ResultItem item) =>
            await resultCollection
            .DeleteOneAsync(x => x.Id == item.Id);

        public async Task RemoveAsync(string id) =>
            await resultCollection
            .DeleteOneAsync(item => item.Id == id);
    }
}
