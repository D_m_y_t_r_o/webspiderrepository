﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountAPI.Models
{
    public class RegisterResult
    {
        public RegisterResult(IEnumerable<string> errors)
        {
            IsValid = false;
            Errors = errors;
        }

        public RegisterResult()
        {
            IsValid = true;
        }

        public bool IsValid { get; }
        public IEnumerable<string> Errors { get; }
    }
}
