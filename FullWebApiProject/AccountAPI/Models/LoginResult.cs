﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountAPI.Models
{
    public class LoginResult
    {
        public LoginResult(User user, string jwtToken)
        {
            IsValid = true;
            User = user;
            JwtToken = jwtToken;
        }

        public LoginResult(string erros)
        {
            IsValid = false;
            ErrorMessage = erros;
        }

        public bool IsValid { get; }
        public User User { get; }
        public string JwtToken { get; }
        public string ErrorMessage { get; }
    }
}
