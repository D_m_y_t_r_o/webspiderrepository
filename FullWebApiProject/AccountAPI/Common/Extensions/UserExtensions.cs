﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace AccountAPI.Common.Extensions
{
    public static class UserExtensions
    {
        public static string GetUserId(this IEnumerable<Claim> claims, string claimUserId) =>
            claims
            .FirstOrDefault(x => x.Type.Equals(claimUserId, StringComparison.InvariantCultureIgnoreCase))?
            .Value;
    }
}
