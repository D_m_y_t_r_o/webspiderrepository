﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using AccountAPI.Common.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using HttpClientExtensions;

namespace AccountAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AuthTaskController : ControllerBase, IDisposable
    {
        private const string taskAPIurl = "https://localhost:5001/api/task/";
        private const string claimUserId = "userId";
        private readonly HttpClient client;

        public AuthTaskController()
        {
            client = new HttpClient();
        }

        [HttpGet("getTasks")]
        public async Task<ActionResult> GetTasks()
        {
            var userId = User.Claims.GetUserId(claimUserId);
            if (userId is null)
                return Unauthorized($"claim '{ claimUserId }' not found");

            var url = taskAPIurl + $"getTasks/{userId}";
            var result = await client.GetRequestContent(url);

            return Ok(result);
        }

        [HttpGet("{taskId}")]
        public async Task<ActionResult> GetTask(int taskId)
        {
            var userId = User.Claims.GetUserId(claimUserId);

            if (userId is null)
                return Unauthorized($"claim '{ claimUserId }' not found");

            var url = taskAPIurl + $"getTask/{taskId}/{userId}";
            var result = await client.GetRequestContent(url);

            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult> PostTask([FromBody] object taskItem)
        {
            var userId = User.Claims.GetUserId(claimUserId);

            if (userId is null)
                return Unauthorized($"claim '{ claimUserId }' not found");

            var url = taskAPIurl + userId;
            var result = await client.PostAsJsonAsync(url, taskItem);

            if (!result.IsSuccessStatusCode)
            {
                var response = await result.Content.ReadAsStringAsync();
                return BadRequest(response);
            }

            return NoContent();
        }

        [HttpPut("{taskId}")]
        public async Task<ActionResult> PutTask(int taskId, [FromBody] object taskItem)
        {
            var userId = User.Claims.GetUserId(claimUserId);

            if (userId is null)
                return Unauthorized($"claim '{ claimUserId }' not found");

            var url = taskAPIurl + $"{taskId}/{userId}";
            var result = await client.PutAsJsonAsync(url, taskItem);

            if (!result.IsSuccessStatusCode)
            {
                var response = await result.Content.ReadAsStringAsync();
                return BadRequest(response);
            }

            return NoContent();
        }

        [HttpDelete("{taskId}")]
        public async Task<ActionResult> DeleteTask(int taskId)
        {
            var userId = User.Claims.GetUserId(claimUserId);

            if (userId is null)
                return Unauthorized($"claim '{ claimUserId }' not found");

            var url = taskAPIurl + $"{taskId}/{userId}";
            var result = await client.DeleteAsync(url);

            if (!result.IsSuccessStatusCode)
            {
                var response = await result.Content.ReadAsStringAsync();
                return BadRequest(response);
            }

            return Ok();
        }

        public void Dispose()
        {
            client?.Dispose();
        }
    }
}