﻿using System.Threading.Tasks;
using AccountAPI.Models;
using AccountAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace AccountAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly ISignInService signInService;

        public AuthController(ISignInService signInService)
        {
            this.signInService = signInService;
        }

        /// <summary>
        /// Get Ok response
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Get() =>
            Ok();

        /// <summary>
        /// Get Bearer 'token' and 'name' for user
        /// </summary>
        /// <returns>Bearer 'token' and 'name'</returns>
        /// <response code="200">Returns Bearer 'token' and 'name'</response>
        /// <response code="400">If name or password not valid</response>
        [HttpPost("login")]
        public async Task<ActionResult> Login([FromBody] LoginViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest("not valid model");

            var loginResult = await signInService.LoginUser(model);

            if (!loginResult.IsValid)
                return BadRequest(loginResult.ErrorMessage);

            return Ok(
                new
                {
                    token = loginResult.JwtToken,
                    name = loginResult.User.UserName
                });
        }

        /// <summary>
        /// Registration
        /// </summary>
        /// <response code="200">Registration complete</response>
        /// <response code="400">If model is not valid</response>
        [HttpPost("register")]
        public async Task<ActionResult> Register([FromBody] RegisterViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest("not valid model");

            var registerResult = await signInService.RegisterUser(model);

            if (!registerResult.IsValid)
                return BadRequest(registerResult.Errors);

            return Ok();
        }

    }
}
