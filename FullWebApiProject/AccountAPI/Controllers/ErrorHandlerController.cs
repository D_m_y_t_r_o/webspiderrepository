﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace AccountAPI.Controllers
{
    public class ErrorHandlerController : Controller
    {
        private const string loggerServcie = "https://localhost:5101/api/logger/";
        private const string loggerApi = "https://localhost:5101/api/logger/logError";

        [Route("Error")]
        [AllowAnonymous]
        public async Task<ActionResult> HandleError()
        {
            var exceptionDetails = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            var exceptionInfo = new
            {
                date = DateTime.Now.ToString(),
                path = exceptionDetails.Path,
                message = exceptionDetails.Error.Message,
                stackTrace = exceptionDetails.Error.StackTrace
            };

            using (var client = new HttpClient())
            {
                try
                {
                    await client.PostAsJsonAsync(loggerApi, exceptionInfo);
                }
                catch { }
            }

            return StatusCode(500, "Some error occured while processing your request. " +
                "The support team is notified and we are working on the fix");
        }
    }
}