﻿using AccountAPI.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AccountAPI
{
    public class AccountDBContext : IdentityDbContext<User>
    {
        public AccountDBContext(DbContextOptions<AccountDBContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
