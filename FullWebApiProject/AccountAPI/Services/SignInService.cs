﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AccountAPI.JwtProperties;
using AccountAPI.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

namespace AccountAPI.Services
{
    public class SignInService : ISignInService
    {
        private const string userIdClaim = "userId";
        private readonly UserManager<User> userManager;
        private readonly JwtSettings settings;

        public SignInService(UserManager<User> userManager, JwtSettings settings)
        {
            this.userManager = userManager;
            this.settings = settings;
        }

        public async Task<LoginResult> LoginUser(LoginViewModel model)
        {
            var user = await userManager.FindByNameAsync(model.Login);

            if (user is null || !(await userManager.CheckPasswordAsync(user, model.Password)))
                return new LoginResult("login or password is wrong");

            var claims = new[]
            {
                new Claim(userIdClaim, user.Id)
            };

            var signKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(settings.SigningKey));
            int expiryInMinutes = int.Parse(settings.ExpiryInMinutes);

            var token = new JwtSecurityToken(
                issuer: settings.Site,
                audience: settings.Site,
                expires: DateTime.UtcNow.AddMinutes(model.RememberMe ? expiryInMinutes * 2 : expiryInMinutes),
                claims: claims,
                signingCredentials: new SigningCredentials(signKey, SecurityAlgorithms.HmacSha256));

            var stringToken = new JwtSecurityTokenHandler().WriteToken(token);

            return new LoginResult(user, stringToken);
        }

        public async Task<RegisterResult> RegisterUser(RegisterViewModel model)
        {
            var user = new User
            {
                UserName = model.Login,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            var result = await userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
                return new RegisterResult(result.Errors.Select(x => x.Description));

            await userManager.AddToRoleAsync(user, "User");

            return new RegisterResult();
        }
    }
}
