﻿using AccountAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountAPI.Services
{
    public interface ISignInService
    {
        Task<LoginResult> LoginUser(LoginViewModel model);

        Task<RegisterResult> RegisterUser(RegisterViewModel model);
    }
}
