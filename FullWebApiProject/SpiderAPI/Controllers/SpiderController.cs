﻿using System.Collections.Generic;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SpiderAPI.Models;
using SpiderAPI.SpiderServiceFolder;
using HttpClientExtensions;

namespace SpiderAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpiderController : ControllerBase, IDisposable
    {
        private const string jobService = "https://localhost:5002/api/jobService";
        private const string getJobFromJobApi = "https://localhost:5002/api/jobService/GetJob";
        private const string sendResultToJobApi = "https://localhost:5002/api/jobService/CreateMany";
        private readonly ISpiderService spiderService;
        private readonly HttpClient client;

        public SpiderController(ISpiderService service)
        {
            spiderService = service;
            client = new HttpClient();
        }

        /// <summary>
        /// Get Ok Response
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetSpiderStatus()
        {
            return Ok();
        }

        /// <summary>
        /// Get job for spider from JobService
        /// </summary>
        /// <response code="200">Returns job to do in string format</response>
        /// <response code="404">if no job for spider</response>
        [HttpGet("GetSpiderJob")]
        public async Task<ActionResult<string>> GetJob()
        {
            if (!await client.UriIsAvailable(jobService))
                return NotFound("Unfortunately, it is not possible to get tasks now");

            var tasks = await client.GetRequestContent(getJobFromJobApi);

            if (tasks is null)
                return NotFound("No Job");

            return Ok(tasks);
        }

        /// <summary>
        /// Get info from site by Xpath
        /// </summary>
        /// <param name="model">model with 'TargetSite' -> Url of the site, 'XPath' -> XPath command</param>
        /// <response code="200">return object with 2 fields: (string)object.body -> result, (bool)object.isValid -> complete successfully</response>
        [HttpPost("GetInfoByXpath")]
        public ActionResult GetInfoByXpath(PageInfo model)
        {
            var getBody = spiderService.GetTextFromSite(model);

            var result = string.IsNullOrEmpty(getBody) ?
                new { body = string.Empty, isValid = false } : new { body = getBody, isValid = true };

            return Ok(result);
        }

        /// <summary>
        /// Procces tasks
        /// </summary>
        /// <response code="200">Returns list of ResultItem</response>
        /// <response code="400">if validation problems</response>
        [HttpPost("InspectPages")]
        public ActionResult<IEnumerable<ResultItem>> InspectPages(IEnumerable<PageInfo> items)
        {
            if (items is null || !items.Any())
                return BadRequest("No items!");

            var result = spiderService.GetResultList(items);

            return Ok(result);
        }

        /// <summary>
        /// Send result(list of ResultItem) to JobService
        /// </summary>
        /// <response code="200">Complete</response>
        /// <response code="400">if validation problems</response>
        /// <response code="404">Failed sending to JobService</response>
        [HttpPost("SendResult")]
        public async Task<ActionResult> SendResult(IEnumerable<ResultItem> resultItems)
        {
            if (resultItems is null || !resultItems.Any())
                return BadRequest("No items!");

            if (!(await client.UriIsAvailable(jobService)))
                return NotFound("Unfortunately, it is not possible to post tasks now");

            var success = await client.PostRequest(sendResultToJobApi, resultItems);

            if (success)
            {
                return Ok();
            }
            else
            {
                return NotFound("Failed to send to JobService");
            }
        }

        public void Dispose() =>
            client?.Dispose();
    }
}
