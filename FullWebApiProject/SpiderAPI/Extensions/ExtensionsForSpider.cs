﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpiderAPI.Extensions
{
    public static class ExtensionsForSpider
    {
        public static string ConcatAllElements<T>(this IEnumerable<T> sequence, string separator) =>
            string.Join(separator, (sequence ?? new T[] { }) );

    }
}
