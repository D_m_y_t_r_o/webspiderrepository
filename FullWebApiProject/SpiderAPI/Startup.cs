﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SpiderAPI.SpiderScheduler;
using SpiderAPI.SpiderServiceFolder;
using Swashbuckle.AspNetCore.Swagger;

namespace SpiderAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ISpiderService, SpiderService>();

            services.AddCors();

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddHostedService<SchedulerForSpider>();

            services.AddSwaggerGen(x =>
            {
                x.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Spider_API",
                    Description = "Procces Tasks",
                    TermsOfService = "None",
                    Contact = new Contact { Name = "Dmytro", Email = "ChiSw" },
                    License = new License { Name = "•ChiSw•", Url = "http://chisw.com" }
                });

                x.IncludeXmlComments("SpiderAPI.xml");
            });

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseExceptionHandler("/Error");

            app.UseCors(option =>
                option.WithOrigins("http://localhost:4200")
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseMvc();

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(x =>
            {
                x.SwaggerEndpoint("/swagger/v1/swagger.json", "Spider_API Version 1");
            });
        }
    }
}
