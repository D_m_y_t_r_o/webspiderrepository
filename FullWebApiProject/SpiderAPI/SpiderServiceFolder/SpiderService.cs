﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using SpiderAPI.Models;
using SpiderAPI.Extensions;

namespace SpiderAPI.SpiderServiceFolder
{
    public class SpiderService : ISpiderService
    {
        private readonly HtmlWeb htmlWeb;

        public SpiderService()
        {
            htmlWeb = new HtmlWeb();
        }

        public HtmlNodeCollection GetNodesFromSite(PageInfo siteInfo)
        {
            var isValidUri = Uri.IsWellFormedUriString(siteInfo.TargetSite, UriKind.Absolute);

            if (!isValidUri)
                return null;

            try
            {
                return htmlWeb
                    .Load(siteInfo.TargetSite)?
                    .DocumentNode?
                    .SelectNodes(siteInfo.XPath);
            }
            catch
            {
                return null;
            }
        }

        public string GetTextFromSite(PageInfo siteInfo) =>
            GetNodesFromSite(siteInfo)?
            .Select(node => node.InnerText.Trim())
            .ConcatAllElements("\n");

        public IEnumerable<ResultItem> GetResultList(IEnumerable<PageInfo> items) =>
            items.AsParallel()
            .Select(x => new ResultItem
            {
                BodyText = GetTextFromSite(x),
                Time = DateTime.UtcNow,
                Url = x.TargetSite,
                XPath = x.XPath
            })
            .Where(x => !string.IsNullOrEmpty(x.BodyText));
    }
}
