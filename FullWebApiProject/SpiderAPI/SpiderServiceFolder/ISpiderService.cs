﻿using HtmlAgilityPack;
using SpiderAPI.Models;
using System.Collections.Generic;

namespace SpiderAPI.SpiderServiceFolder
{
    public interface ISpiderService
    {
        HtmlNodeCollection GetNodesFromSite(PageInfo siteInfo);
        string GetTextFromSite(PageInfo siteInfo);
        IEnumerable<ResultItem> GetResultList(IEnumerable<PageInfo> items);
    }
}
