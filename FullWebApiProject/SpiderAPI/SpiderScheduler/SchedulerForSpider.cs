﻿using Microsoft.Extensions.Hosting;
using SpiderAPI.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using HttpClientExtensions;
using System.Net.Http;

namespace SpiderAPI.SpiderScheduler
{
    public class SchedulerForSpider : IHostedService, IDisposable
    {
        private const string getSpiderJob = "https://localhost:5003/api/Spider/GetSpiderJob";
        private const string doSpiderJob = "https://localhost:5003/api/Spider/InspectPages";
        private const string sendSpiderResult = "https://localhost:5003/api/Spider/SendResult";
        private Timer timer;
        private readonly HttpClient client;

        public SchedulerForSpider()
        {
            client = new HttpClient();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            timer = new Timer(
                async(state) => await RepeatJob(),
                null,
                TimeSpan.Zero,
                TimeSpan.FromSeconds(15)); // change time

            return Task.CompletedTask;
        }

        private async Task RepeatJob()
        {
            var tasksForSpider = await client
                .GetRequestContent<IEnumerable<PageInfo>>(getSpiderJob);

            if (tasksForSpider is null)
                return;

            var resultData = await client
                .PostRequestWithAnswer<IEnumerable<PageInfo>, IEnumerable<ResultItem>>(doSpiderJob, tasksForSpider);

            if (resultData is null)
                return;

            _ = await client.PostRequest(sendSpiderResult, resultData);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            timer?.Dispose();
            client?.Dispose();
        }
    }
}
