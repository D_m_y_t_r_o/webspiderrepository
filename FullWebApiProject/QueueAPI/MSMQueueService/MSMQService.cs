﻿using Experimental.System.Messaging;
using QueueAPI.QueueSettingsFolder;
using System;

namespace QueueAPI.MSMQueueService
{
    public class MSMQService : IMSMQService, IDisposable
    {
        private readonly MessageQueue queue;

        public MSMQService(IQueueSettings settings)
        {
            var path = settings.ConnectionString;

            if (!MessageQueue.Exists(path))
            {
                MessageQueue.Create(path);
            }

            queue = new MessageQueue(path)
            {
                Formatter = new XmlMessageFormatter(new Type[] { typeof(string) })
            };
        }

        public void EnqueueTasks(string tasks) =>
            queue.Send(tasks);

        public string DequeueTasks()
        {
            try
            {
                var messageFromQueue = queue.Receive(TimeSpan.FromMilliseconds(10));

                var tasks = messageFromQueue?.Body?.ToString();

                return tasks;
            }
            catch
            {
                return null;
            }
        }

        public void Dispose() =>
            queue?.Dispose();
    }
}
