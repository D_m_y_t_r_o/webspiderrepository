﻿using System;

namespace QueueAPI.MSMQueueService
{
    public interface IMSMQService : IDisposable
    {
        void EnqueueTasks(string tasks);

        string DequeueTasks();
    }
}
