﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using QueueAPI.MSMQueueService;
using QueueAPI.QueueSettingsFolder;
using Swashbuckle.AspNetCore.Swagger;

namespace QueueAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<QueueSettings>(
                Configuration.GetSection(nameof(QueueSettings)));

            services.AddSingleton<IQueueSettings>(sp =>
                sp.GetRequiredService<IOptions<QueueSettings>>().Value);

            services.AddScoped<IMSMQService, MSMQService>();

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(x =>
            {
                x.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Queue_API",
                    Description = "Work with MSMQ",
                    TermsOfService = "None",
                    Contact = new Contact { Name = "Dmytro", Email = "ChiSw" },
                    License = new License { Name = "•ChiSw•", Url = "http://chisw.com" }
                });

                x.IncludeXmlComments("QueueAPI.xml");
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseExceptionHandler("/Error");
            app.UseMvc();

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(x =>
            {
                x.SwaggerEndpoint("/swagger/v1/swagger.json", "Queue_API Version 1");
            });
        }
    }
}
