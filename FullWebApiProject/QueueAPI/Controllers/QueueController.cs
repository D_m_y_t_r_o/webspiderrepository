﻿using Microsoft.AspNetCore.Mvc;
using QueueAPI.MSMQueueService;
using System;

namespace QueueAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QueueController : ControllerBase, IDisposable
    {
        private readonly IMSMQService service;
        public QueueController(IMSMQService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Get Ok response
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Get() =>
            Ok();

        /// <summary>
        /// Get string message from MSMQ
        /// </summary>
        /// <returns>string</returns>
        /// <response code="200">Returns message in string format</response>
        /// <response code="400">If no messages in MSMQ</response>
        [HttpGet("GetTasks")]
        public ActionResult<string> DequeueTasks()
        {
            var result = service.DequeueTasks();

            if (string.IsNullOrEmpty(result))
                return NotFound("No job to do");

            return Ok(result);
        }

        /// <summary>
        /// Send message to MSMQ
        /// </summary>
        /// <param name="tasks">message in string format</param>
        /// <response code="200">Complete</response>
        /// <response code="400">If validation problems</response>
        [HttpPost]
        public ActionResult EnqueueTasks([FromBody] string tasks)
        {
            if (string.IsNullOrEmpty(tasks))
                return BadRequest("Not valid model");

            service.EnqueueTasks(tasks);

            return Ok();
        }

        public void Dispose()
        {
            service?.Dispose();
        }
    }
}
