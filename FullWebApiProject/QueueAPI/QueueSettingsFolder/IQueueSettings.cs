﻿
namespace QueueAPI.QueueSettingsFolder
{
    public interface IQueueSettings
    {
        string ConnectionString { get; set; }
    }
}
