﻿using LoggerWebAPI.Models;
using LoggerWebAPI.Services;
using Microsoft.AspNetCore.Mvc;
using NLog;
using System.Threading.Tasks;

namespace LoggerWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoggerController : Controller
    {
        private readonly Logger logger;
        private readonly IElasticSearchService elasticSearch;

        public LoggerController(IElasticSearchService elasticSearch)
        {
            logger = LogManager.GetLogger("allWebApiLogger");
            this.elasticSearch = elasticSearch;
        }

        [HttpGet]
        public ActionResult Get() =>
            Ok();

        [HttpGet("getAllInfos")]
        public async Task<ActionResult> GetAllInfo()
        {
            var result = await elasticSearch.Search<LogInfo>(x => x.Index("infologs").MatchAll());

            return Ok(result.Documents);
        }

        [HttpGet("getAllErrors")]
        public async Task<ActionResult> GetAllErrors()
        {
            var result = await elasticSearch.Search<LogError>(x => x.Index("errorlogs").MatchAll());

            return Ok(result.Documents);
        }

        [HttpPost("logError")]
        public async Task<ActionResult> LogError([FromBody] LogError model)
        {
            logger.Error(
                $"\nDate: '{model.Date}'\n" +
                $"The path: '{model.Path}'\n" +
                $"Message: {model.Message}\n" +
                $"Stack Trace: {model.StackTrace}\n\n");

            await elasticSearch.PushErrorMessage(model);

            return NoContent();
        }

        [HttpPost("logInfo")]
        public async Task<ActionResult> LogTrace([FromBody] LogInfo model)
        {

            logger.Info(
                $"\nDate: {model.Date}\n" +
                $"Level: '{model.Level}'\n" +
                $"Message: {model.Message}\n\n");

            await elasticSearch.PushInfoMessage(model);

            return NoContent();
        }
    }
}
