﻿
namespace LoggerWebAPI.Models
{
    public class LogInfo
    {
        public string Date { get; set; }
        public string Level { get; set; }
        public string Message { get; set; }
    }
}
