﻿
namespace LoggerWebAPI.Models
{
    public class LogError
    {
        public string Date { get; set; }
        public string Path { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
    }
}
