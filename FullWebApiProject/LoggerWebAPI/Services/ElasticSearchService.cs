﻿using LoggerWebAPI.Models;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoggerWebAPI.Services
{
    public class ElasticSearchService : IElasticSearchService
    {
        private readonly ElasticClient client;

        public ElasticSearchService()
        {
            var node = new Uri("http://localhost:9200");
            var settings = new ConnectionSettings(node);
            client = new ElasticClient(settings);
        }

        public async Task<IndexResponse> PushErrorMessage<T>(T message) where T : LogError =>
            await client.IndexAsync(message, x => x.Index("errorlogs"));

        public async Task<IndexResponse> PushInfoMessage<T>(T message) where T : LogInfo =>
            await client.IndexAsync(message, x => x.Index("infologs"));

        public async Task<ISearchResponse<T>> Search<T>(Func<SearchDescriptor<T>, ISearchRequest> predicate) where T: class =>
            await client.SearchAsync(predicate);
    }
}
