﻿using LoggerWebAPI.Models;
using Nest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LoggerWebAPI.Services
{
    public interface IElasticSearchService
    {
        Task<IndexResponse> PushInfoMessage<T>(T message) where T: LogInfo;
        Task<IndexResponse> PushErrorMessage<T>(T message) where T: LogError;

        Task<ISearchResponse<T>> Search<T>(Func<SearchDescriptor<T>, ISearchRequest> predicate) where T: class;

    }
}
