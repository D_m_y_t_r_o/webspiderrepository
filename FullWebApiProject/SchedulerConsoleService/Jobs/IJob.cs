﻿using System.Threading.Tasks;

namespace SchedulerConsoleService.Jobs
{
    public interface IJob
    {
        Task TransferInfoFromSqlToQueue();
    }
}
