﻿using System.Net.Http;
using System.Threading.Tasks;
using HttpClientExtensions;

namespace SchedulerConsoleService.Jobs
{
    class Job : IJob
    {
        private const string taskApi = "https://localhost:5001/api/task/";
        private const string getTasksFromTaskApi = "https://localhost:5001/api/task/GetSchedulerTasks";
        private const string postTasksToQueue = "https://localhost:5005/api/Queue";

        public async Task TransferInfoFromSqlToQueue()
        {
            using (var client = new HttpClient())
            {
                if (!(await client.UriIsAvailable(taskApi)))
                    return;

                var getTasks = await client.GetAsync(getTasksFromTaskApi); //GetSchedulerTasks

                if (!getTasks.IsSuccessStatusCode)
                    return;

                var tasks = await getTasks.Content.ReadAsStringAsync();
                var response = await client.PostAsJsonAsync(postTasksToQueue, tasks);
            }
        }
    }
}
