﻿using SchedulerConsoleService.Jobs;
using System;
using System.Threading;

namespace SchedulerConsoleService
{
    class Program
    {
        static void Main(string[] args)
        {
            IJob job = new Job();

            var time = new Timer(
                async (state) => await job.TransferInfoFromSqlToQueue(),
                null,
                TimeSpan.Zero,
                TimeSpan.FromSeconds(15));

            Console.ReadKey();
        }
    }
}
