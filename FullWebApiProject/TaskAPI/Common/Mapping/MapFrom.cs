﻿using AutoMapper;

namespace TaskAPI.Mapping
{
    public abstract class MapFrom<T>
    {
        public virtual void Mapping(Profile profile) => profile.CreateMap(typeof(T), GetType());
    }
}
