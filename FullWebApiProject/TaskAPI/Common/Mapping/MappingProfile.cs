﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TaskAPI.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            ApplyMappings(Assembly.GetExecutingAssembly());
        }

        private void ApplyMappings(Assembly assembly)
        {
            var types = GetListOfDtoTypes(assembly);

            types.ForEach(type =>
            {
                var instance = Activator.CreateInstance(type);
                type.GetMethod("Mapping")?
                .Invoke(instance, new object[] { this });
            });
        }

        private List<Type> GetListOfDtoTypes(Assembly assembly) =>
            assembly.GetTypes()
            .Where(x => x.BaseType != null && 
                x.BaseType.IsGenericType && 
                x.BaseType.GetGenericTypeDefinition() == typeof(MapFrom<>))
            .ToList();
    }
}
