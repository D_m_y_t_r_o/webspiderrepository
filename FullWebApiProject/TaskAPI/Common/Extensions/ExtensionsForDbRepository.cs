﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskAPI.Models;

namespace TaskAPI.DbRepository
{
    public class ExtensionsForDbRepository
    {
        public static Func<TaskItem, bool> PredicateForTaskItem = item =>
        {
            double timeNowInMinutes = DateTime.Now.Hour * 60 + DateTime.Now.Minute;
            var time = (timeNowInMinutes - item.TimeStart.Hour * 60 - item.TimeStart.Minute) % item.ExecuteFrequency.TotalMinutes;

            return (item.TimeStart <= DateTime.Now) &&
                        (time >= 0) && (time <= 1);
        };
    }
}
