﻿using Microsoft.EntityFrameworkCore;
using TaskAPI.Models;

namespace TaskAPI
{
    internal partial class TaskDBContext : DbContext
    {
        public TaskDBContext(DbContextOptions<TaskDBContext> options) : base(options) { }

        public DbSet<TaskItem> TaskItems { get; set; }
    }
}
