﻿
using AutoMapper;
using TaskAPI.Mapping;

namespace TaskAPI.Models
{
    public class PageInfoDTO: MapFrom<TaskItem>
    {
        public string TargetSite { get; set; }
        public string XPath { get; set; }
    }
}
