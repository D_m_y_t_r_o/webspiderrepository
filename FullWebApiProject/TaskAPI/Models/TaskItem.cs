﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaskAPI.Models
{
    [Table("TaskItems")]
    public partial class TaskItem
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "You must input site address")]
        [MaxLength(300)]
        [Column(TypeName = "nvarchar(300)")]
        public string TargetSite { get; set; }

        [Required(ErrorMessage = "You must input XPath")]
        [MaxLength(300)]
        [Column(TypeName = "nvarchar(300)")]
        public string XPath { get; set; }

        [Required(ErrorMessage = "You must input TimeStart")]
        public DateTime TimeStart { get; set; }

        [Required(ErrorMessage = "You must input ExecuteFrequency")]
        [RegularExpression(@"^(([01]\d)|(2[0-3])):[0-5]\d:[0-5]\d$")]
        public TimeSpan ExecuteFrequency { get; set; }

        [JsonIgnore]
        [ForeignKey("User")]
        [MaxLength(450)]
        [Column(TypeName = "nvarchar(450)")]
        public string UserId { get; set; }

        [JsonIgnore]
        public virtual User User { get; set; }
    }
}
