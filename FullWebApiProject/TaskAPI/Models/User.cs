﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaskAPI.Models
{
    [Table("AspNetUsers")]
    public class User : IdentityUser
    {
        public User()
        {
            TaskItems = new List<TaskItem>();
        }

        public virtual ICollection<TaskItem> TaskItems { get; set; }
    }

}
