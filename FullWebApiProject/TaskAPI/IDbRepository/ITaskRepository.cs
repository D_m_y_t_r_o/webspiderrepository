﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskAPI.Models;

namespace TaskAPI.IDbRepository
{
    public interface ITaskRepository
    {
        IQueryable<TaskItem> GetTaskItemsWithoutTracking { get; }
        Task<IEnumerable<TaskItem>> GetAllAsync();
        Task<IEnumerable<TaskItem>> GetTasksForUser(string userId);
        TaskItem GetById(int id);
        TaskItem GetByIdWithTracking(int id);
        Task<TaskItem> GetByIdAsync(int id);
        Task<TaskItem> GetByIdAsyncWithTracking(int id);
        Task<IEnumerable<PageInfoDTO>> GetSchedulerTasks(Func<TaskItem, bool> func);
        Task AddAsync(TaskItem taskItem);
        void Delete(TaskItem id);
        TaskItem Update(TaskItem taskItem);
        Task<bool> AddAndSaveAsync(TaskItem taskItem);
        Task<bool> DeleteAndSaveAsync(TaskItem id);
        Task<bool> UpdateAndSaveAsync(TaskItem taskItem);
        bool ItemExist(int id);
        Task<int> CountAsync();
        Task<bool> SaveAsync();
    }
}
