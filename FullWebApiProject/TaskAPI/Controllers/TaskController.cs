﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TaskAPI.DbRepository;
using TaskAPI.IDbRepository;
using TaskAPI.Models;

namespace TaskAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskRepository taskRepository;

        public TaskController(ITaskRepository repository)
        {
            taskRepository = repository;
        }

        /// <summary>
        /// Get Ok response
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Get()
        {
            return Ok();
        }

        /// <summary>
        /// Get list of PageInfo for scheduler
        /// </summary>
        /// <response code="200">Returns list of PageInfo</response>
        /// <response code="404">If no job for scheduler at this time</response>
        [HttpGet("GetSchedulerTasks")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<PageInfoDTO>>> GetSchedulerTasks()
        {
            var listResult = await taskRepository.GetSchedulerTasks(ExtensionsForDbRepository.PredicateForTaskItem);

            if (!listResult.Any())
                return NotFound("No items to do");

            return Ok(listResult);
        }

        /// <summary>
        /// Get Tasks for registered user
        /// </summary>
        /// <response code="200">Returns the list of TaskItem</response>
        [HttpGet("getTasks/{userId}")]
        public async Task<ActionResult<IEnumerable<TaskItem>>> GetTasksForUser(string userId)
        {
            var tasksForUser = await taskRepository.GetTasksForUser(userId);
            
            return Ok(tasksForUser);
        }

        /// <summary>
        /// Get Task for registered user
        /// </summary>
        /// <param name="taskId">id of the TaskItem</param>
        /// <param name="userId">id of the User</param>
        /// <response code="200">Returns TaskItem</response>
        /// <response code="400">If TaskItem not found in DataBase</response>
        /// <response code="403">If TaskItem not belongs to user</response>
        [HttpGet("getTask/{taskId}/{userId}")]
        public async Task<ActionResult<TaskItem>> GetTask(int taskId, string userId)
        {
            var taskItem = await taskRepository.GetByIdAsync(taskId);

            if (taskItem is null)
                return BadRequest("Not found in DataBase");

            if (!taskItem.UserId.Equals(userId))
                return Forbid("You have not a permission to this task");

            return Ok(taskItem);
        }

        /// <summary>
        /// Create new TaskItem
        /// </summary>
        /// <param name="userId">id of the User</param>
        /// <param name="taskItem">instance of TaskItem</param>
        /// <response code="200">Return created taskItem</response>
        /// <response code="400">TaskItem model not valid</response>
        [HttpPost("{userId}")]
        public async Task<ActionResult<TaskItem>> PostTask(string userId, [FromBody] TaskItem taskItem)
        {
            if (!ModelState.IsValid)
                return BadRequest("Validation problem");

            taskItem.UserId = userId;
            bool success = await taskRepository.AddAndSaveAsync(taskItem);

            if (!success)
                return BadRequest("Adding item failed on save");

            return Ok(taskItem);
        }

        /// <summary>
        /// Edit Task
        /// </summary>
        /// <param name="taskId">id of the TaskItem</param>
        /// <param name="userId">id of the User</param>
        /// <param name="item">instance of TaskItem</param>
        /// <response code="200">Return edited taskItem</response>
        /// <response code="400">If validation problems or not found in dataBase</response>
        /// <response code="403">If TaskItem not belongs to user</response>
        [HttpPut("{taskId}/{userId}")]
        public async Task<ActionResult<TaskItem>> PutTask(int taskId, string userId, [FromBody] TaskItem item)
        {
            if (!ModelState.IsValid || taskId != item.Id)
                return BadRequest("Validation problem");

            item.UserId = userId;
            var dbItem = await taskRepository.GetByIdAsync(taskId);

            if (dbItem is null)
                return BadRequest("not found item");

            if (!userId.Equals(dbItem.UserId))
                return Forbid("You have not a permission to this task");

            bool success = await taskRepository.UpdateAndSaveAsync(item);

            if (!success)
                return BadRequest("No updates have been written to the database");

            return Ok(item);
        }

        /// <summary>
        /// Delete Task
        /// </summary>
        /// <param name="taskId">id of the TaskItem</param>
        /// <param name="userId">id of the User</param>
        /// <response code="204">Deleting complete</response>
        /// <response code="400">Failed to save</response>
        /// <response code="403">TaskItem not belongs to user</response>
        /// <response code="404">Not found in dataBase</response>
        [HttpDelete("{taskId}/{userId}")]
        public async Task<IActionResult> DeleteTask(int taskId, string userId)
        {
            var dbTask = await taskRepository.GetByIdAsyncWithTracking(taskId);

            if (dbTask is null)
                return NotFound("task not found");

            if (!userId.Equals(dbTask.UserId))
                return Forbid("You have not a permission to delete this task");

            bool success = await taskRepository.DeleteAndSaveAsync(dbTask);

            if (!success)
                return BadRequest("Deleting item failed on save");

            return NoContent();
        }
    }
}
