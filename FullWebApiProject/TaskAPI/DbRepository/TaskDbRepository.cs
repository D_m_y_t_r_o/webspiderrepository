﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskAPI.IDbRepository;
using TaskAPI.Models;

namespace TaskAPI.DbRepository
{
    internal class TaskDbRepository: ITaskRepository
    {
        private readonly TaskDBContext context;
        private readonly IMapper mapper;

        public TaskDbRepository(TaskDBContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public IQueryable<TaskItem> GetTaskItemsWithoutTracking =>
            context
            .TaskItems
            .AsNoTracking();

        public async Task<IEnumerable<TaskItem>> GetAllAsync() =>
            await GetTaskItemsWithoutTracking
                .ToListAsync();

        public async Task<IEnumerable<TaskItem>> GetTasksForUser(string userId) =>
            await GetTaskItemsWithoutTracking.Where(x => x.UserId.Equals(userId))
                .ToListAsync();

        public TaskItem GetById(int id) =>
            GetTaskItemsWithoutTracking
            .FirstOrDefault(x => x.Id == id);

        public TaskItem GetByIdWithTracking(int id) =>
            context.TaskItems
            .FirstOrDefault(x => x.Id == id);

        public async Task<TaskItem> GetByIdAsync(int id) =>
           await GetTaskItemsWithoutTracking
            .FirstOrDefaultAsync(x => x.Id == id);

        public async Task<TaskItem> GetByIdAsyncWithTracking(int id) =>
            await context.TaskItems
            .FirstOrDefaultAsync(x => x.Id == id);

        public async Task<IEnumerable<PageInfoDTO>> GetSchedulerTasks(Func<TaskItem, bool> predicate) =>
            await GetTaskItemsWithoutTracking
            .Where(x => predicate(x))
            .ProjectTo<PageInfoDTO>(mapper.ConfigurationProvider)
            .ToListAsync();

        public async Task AddAsync(TaskItem taskItem) =>
            await context
            .TaskItems
            .AddAsync(taskItem);

        public async Task<bool> AddAndSaveAsync(TaskItem taskItem)
        {
            await context.TaskItems.AddAsync(taskItem);

            return await this.SaveAsync();
        }

        public TaskItem Update(TaskItem taskItem)
        {
            context.TaskItems.Update(taskItem);

            return taskItem;
        }

        public async Task<bool> UpdateAndSaveAsync(TaskItem taskItem)
        {
            context.TaskItems.Update(taskItem);

            return await this.SaveAsync();
        }

        public void Delete(TaskItem taskItem) =>
            context.TaskItems.Remove(taskItem);

        public async Task<bool> DeleteAndSaveAsync(TaskItem taskItem)
        {
            context.TaskItems.Remove(taskItem);

            return await this.SaveAsync();
        }

        public bool ItemExist(int id) =>
            GetTaskItemsWithoutTracking
            .Any(x => x.Id == id);

        public async Task<int> CountAsync() =>
            await GetTaskItemsWithoutTracking.CountAsync();

        public async Task<bool> SaveAsync() =>
            await context.SaveChangesAsync() > 0;

    }
}
