import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { InfoDetail } from 'src/app/shared/task-service/info-detail.model';
import { InfoDetailService } from 'src/app/shared/task-service/info-detail.service';

@Component({
  selector: 'app-info-detail-list',
  templateUrl: './info-detail-list.component.html',
  styles: [],

})

export class InfoDetailListComponent implements OnInit {

  public dataSource: MatTableDataSource<InfoDetail>;
  public displayedColumns: string[] = ['Link', 'XPath', 'TimeStart', 'Period', 'Id'];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private service: InfoDetailService, private toastr: ToastrService) { }

  ngOnInit() {
    this.service.onSubmit.subscribe(() => {
      this.updateTable();
    });
    this.updateTable();
  }

  public updateTable() {
    this.service.AllInfoDetails().subscribe(
      data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
      },
      err => {
        this.toastr.error(err.error instanceof ProgressEvent ? 'Failed to load taskList. Try again later' : err.error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  populateForm(inf: InfoDetail) {
    this.service.populateReactiveForm(inf);
  }

  onDelete(Id) {
    if (confirm('Are you sure to delete this record ?')) {
      this.service.deleteInfoDetail(Id)
        .subscribe(res => {
          this.updateTable();
          this.toastr.warning('Deleted successfully', 'Info Detail');
        },
          err => {
            console.log(err);
          });
    }
  }
}
