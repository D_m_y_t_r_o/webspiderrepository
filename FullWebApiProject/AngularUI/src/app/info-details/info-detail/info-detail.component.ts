import { Component, OnInit } from '@angular/core';
import { InfoDetailService } from 'src/app/shared/task-service/info-detail.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators, ValidatorFn, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-info-detail',
  templateUrl: './info-detail.component.html',
  styles: []
})
export class InfoDetailComponent implements OnInit {

  fieldValidators: ValidatorFn[] =
    [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(300)
    ];

  formsErrors = {
    TargetSite: '',
    XPath: '',
    TimeStart: '',
    ExecuteFrequency: ''
  };

  validationMessages = {
    TargetSite: {
      required: 'TargetSite is required',
      minlength: 'TargetSite must be greater than 6 characters',
      maxlength: 'TargetSite must be less than 300 characters',
    },
    XPath: {
      required: 'XPath is required.',
      minlength: 'XPath must be greater than 6 characters',
      maxlength: 'XPath must be less than 300 characters',
    },
    TimeStart: {
      required: 'TimeStart is required',
    },
    ExecuteFrequency: {
      required: 'ExecuteFrequency is required',
      pattern: 'ExecuteFrequency must be like "00:00:00"'
    }
  };

  constructor(private service: InfoDetailService,
              private toastr: ToastrService,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.service.formDataReactive = this.fb.group({
      Id: [0, Validators.required],
      TargetSite: ['', this.fieldValidators],
      XPath: ['', this.fieldValidators],
      TimeStart: ['', Validators.required],
      ExecuteFrequency: ['', Validators.pattern('^(([01]\\d)|(2[0-3])):[0-5]\\d:[0-5]\\d$')]
    });
    this.service.formDataReactive.valueChanges.subscribe(data => this.validateErrors(this.service.formDataReactive));
    this.service.buttonName = 'Submit';
  }

  validateErrors(group: FormGroup = this.service.formDataReactive): void {
    Object.keys(group.controls).forEach(key => {
      const value = group.get(key);
      this.formsErrors[key] = '';
      if (value && !value.valid && (value.touched || value.dirty)) {
        const message = this.validationMessages[key];
        for (const error in value.errors) {
          if (error) {
            this.formsErrors[key] += message[error];
          }
        }
      }
    });
  }

  validateProperty(key: string): boolean {
    const property = this.service.formDataReactive.get(key);
    return (property.valid && (property.touched || property.dirty));
  }

  touchedOrDirty(key: string): boolean {
    const prop = this.service.formDataReactive.get(key);
    return (prop.touched || prop.dirty);
  }

  resetReactiveForm() {
    this.service.buttonName = 'Submit';
    for (const key in this.formsErrors) {
      if (key) {
        this.formsErrors[key] = '';
      }
    }

    for (const key in this.service.formDataReactive.controls) {
      if (key) {
        this.service.formDataReactive.get(key).reset(key === 'Id' ? 0 : '');
      }
    }
  }

  onReactiveSubmit() {
    if (!this.service.formDataReactive.valid) {
      this.toastr.error('validation problems');
      return;
    }
    if (this.service.formDataReactive.get('Id').value === 0) {
      this.insertRecordReactive();
    } else {
      this.updateRecordReactive();
    }
  }

  insertRecordReactive() {
    this.service.postInfoDetailReactive().subscribe(
      res => {
        this.service.onSubmit.emit(null);
        this.toastr.success('Inserted successfully', 'Info. ADDED');
        this.resetReactiveForm();
      },
      err => {
        console.log(err);
      });
  }

  updateRecordReactive() {
    this.service.putInfoDetailReactive().subscribe(
      res => {
        this.service.onSubmit.emit(null);
        this.toastr.warning('Updated successfully', 'INF. ADDED');
      },
      err => {
        console.log(err);
      });
  }

}
