import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InfoDetailsComponent } from '../info-details/info-details.component';
import { UserComponent } from '../user/user.component';
import { LoginComponent } from '../user/login/login.component';
import { RegistrationComponent } from '../user/registration/registration.component';
import { AuthGuard } from '../auth/auth.guard';
import { CheckXpathComponent } from '../check-xpath/check-xpath.component';

const appRoutes: Routes = [
  {
    path: '',
    component: InfoDetailsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'xpath',
    component: CheckXpathComponent
  },
  {
    path: 'user',
    component: UserComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'registration', component: RegistrationComponent }
    ]
  }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, { onSameUrlNavigation: 'ignore' })
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }
