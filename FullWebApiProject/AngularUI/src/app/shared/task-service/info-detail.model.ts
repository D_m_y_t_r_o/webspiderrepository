export interface InfoDetail {
    Id: number;
    TargetSite: string;
    XPath: string;
    TimeStart: Date;
    ExecuteFrequency: string;
}
