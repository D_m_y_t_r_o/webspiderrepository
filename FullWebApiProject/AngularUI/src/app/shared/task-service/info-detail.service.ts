import { Injectable, ViewChild, EventEmitter } from '@angular/core';
import { InfoDetail } from './info-detail.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})

export class InfoDetailService {
  formDataReactive: FormGroup;
  public onSubmit = new EventEmitter<any>();
  buttonName: string;
  readonly rootURL = 'https://localhost:5006/api/AuthTask/';

  constructor(private http: HttpClient) { }

  postInfoDetailReactive() {
    return this.http.post(this.rootURL, this.formDataReactive.value);
  }

  putInfoDetailReactive() {
    return this.http.put(this.rootURL + this.formDataReactive.value.Id, this.formDataReactive.value);
  }

  deleteInfoDetail(id: number) {
    return this.http.delete(this.rootURL + id);
  }

  AllInfoDetails(): Observable<InfoDetail[]> {
    return this.http.get<InfoDetail[]>(this.rootURL + 'getTasks');
  }

  populateReactiveForm(info: InfoDetail) {
    this.formDataReactive.setValue({
      Id: info.Id,
      TargetSite: info.TargetSite,
      XPath: info.XPath,
      TimeStart: info.TimeStart,
      ExecuteFrequency: info.ExecuteFrequency
    });
    this.buttonName = 'Update';
  }

}
