import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class XpathService {
  readonly rootUrl = 'https://localhost:5003/api/spider/';
  xpathReactiveForm: FormGroup;

  constructor(private httpClient: HttpClient) { }

  checkXpathInfo() {
    return this.httpClient.post(this.rootUrl + 'GetInfoByXpath/', this.xpathReactiveForm.value);
  }

}
