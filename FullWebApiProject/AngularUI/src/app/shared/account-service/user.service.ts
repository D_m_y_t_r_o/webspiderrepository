import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup } from '@angular/forms';

@Injectable({
    providedIn: 'root'
})

export class UserService {
    formLogindDataReactive: FormGroup;
    formRegisterDataReactive: FormGroup;

    readonly rootURL = 'https://localhost:5006/api/auth';
    constructor(private http: HttpClient) { }

    postInfoLogin() {
        return this.http.post(this.rootURL + '/login', this.formLogindDataReactive.value);
    }

    postInfoRegister() {
        return this.http.post(this.rootURL + '/register', this.formRegisterDataReactive.value);
    }

}

