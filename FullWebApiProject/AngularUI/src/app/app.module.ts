import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AngularmaterialModule } from './material/angularmaterial/angularmaterial.module';
import { AppRoutingModule } from './app-routing/app-routing.module';

import { AppComponent } from './app.component';
import { InfoDetailsComponent } from './info-details/info-details.component';
import { InfoDetailComponent } from './info-details/info-detail/info-detail.component';
import { InfoDetailListComponent } from './info-details/info-detail-list/info-detail-list.component';
import { InfoDetailService } from './shared/task-service/info-detail.service';
import { UserService } from './shared/account-service/user.service';
import { LoginComponent } from './user/login/login.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { UserComponent } from './user/user.component';
import { AuthInterceptor } from './auth/auth.interceptor';
import { CheckXpathComponent } from './check-xpath/check-xpath.component';

@NgModule({
  declarations: [
    AppComponent,
    InfoDetailsComponent,
    InfoDetailComponent,
    InfoDetailListComponent,
    LoginComponent,
    RegistrationComponent,
    UserComponent,
    CheckXpathComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularmaterialModule,
    ReactiveFormsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    InfoDetailService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
