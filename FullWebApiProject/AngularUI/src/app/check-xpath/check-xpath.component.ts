import { Component, OnInit } from '@angular/core';
import { XpathService } from '../shared/xpath-service/xpath.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-check-xpath',
  templateUrl: './check-xpath.component.html',
  styleUrls: []
})
export class CheckXpathComponent implements OnInit {

  XpathInfo = {
    Site: 'some site',
    Xpath: 'some XPath',
    Body: 'example info...'
};
  isValid = true;
  showLoading = false;
  errors = '';

  constructor(private service: XpathService,
              private toastr: ToastrService,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.service.xpathReactiveForm = this.fb.group({
      TargetSite: ['', Validators.required],
      XPath: ['', Validators.required]
    });
  }

  validateProperty(key: string): boolean {
    const prop = this.service.xpathReactiveForm.get(key);
    return (!prop.valid && (prop.touched || prop.dirty));
  }

  onSubmitXpathForm() {
    if (!this.service.xpathReactiveForm.valid) {
      return;
    }

    this.errors = '';
    this.isValid = false;
    this.showLoading = true;
    this.service.checkXpathInfo().subscribe(
      (res: any) => {
        this.showLoading = false;
        this.isValid = res.isValid;
        this.XpathInfo.Body = res.body;
        this.XpathInfo.Site = this.service.xpathReactiveForm.get('TargetSite').value;
        this.XpathInfo.Xpath = this.service.xpathReactiveForm.get('XPath').value;
        this.errors = this.isValid ? '' : 'Site or Xpath is not valid';
      },
      err => {
        this.toastr.error(err.error);
        console.log(err);
        this.showLoading = false;
      }
    );
  }

}
