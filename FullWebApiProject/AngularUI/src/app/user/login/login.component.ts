import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/account-service/user.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {

  showLoading = false;

  constructor(private service: UserService, private toastr: ToastrService,
              private router: Router, private appComp: AppComponent,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.service.formLogindDataReactive = this.fb.group({
      Login: ['', Validators.required],
      Password: ['', Validators.required],
      RememberMe: [false],
    });
  }

  loginUser() {
    this.showLoading = true;
    this.service.postInfoLogin().subscribe(
      (res: any) => {
        this.showLoading = false;
        localStorage.setItem('token', res.token);
        localStorage.setItem('name', res.name);
        this.router.navigateByUrl('');
        this.appComp.updateHeader();
        this.toastr.success('Welcome ' + res.name);
      },
      err => {
        this.showLoading = false;
        this.toastr.error(err.error instanceof ProgressEvent ? 'Server error occured. Try again later' : err.error);
        console.log(err);
      });
  }

}
