import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/account-service/user.service';
import { FormBuilder, Validators, AbstractControl, FormGroup, ValidationErrors, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styles: []
})
export class RegistrationComponent implements OnInit {

  showLoading = false;

  formsErrors = {
    login: '',
    password: '',
    confirmPassword: ''
  };

  validationMessages = {
    login: {
      required: 'Login is required',
      minlength: 'Login must be greater than 3 characters',
      maxlength: 'Login must be less than 100 characters',
    },
    password: {
      required: 'Password is required.',
      minlength: 'Password must be greater than 6 characters',
      maxlength: 'Password must be less than 100 characters',
    },
    confirmPassword: {
      required: 'ConfirmPassword is required',
      comparePass: 'Passwords dont match'
    }
  };

  constructor(private service: UserService,
              private toastr: ToastrService,
              private router: Router,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.service.formRegisterDataReactive = this.fb.group({
      login: ['', [Validators.minLength(3), Validators.maxLength(100)]],
      password: ['', [Validators.minLength(6), Validators.maxLength(100)]],
      confirmPassword: ['', [this.comparePass]]
    });

    this.service.formRegisterDataReactive.valueChanges.subscribe(data => {
      this.validateErrors(this.service.formRegisterDataReactive);
    });
  }

  comparePass(control: AbstractControl) {
    if (control.parent) {
      const pass = control.parent.get('password');
      return pass.value === control.value ? null : { comparePass: true };
    }
    return null;
  }

  validateErrors(group: FormGroup = this.service.formRegisterDataReactive): void {
    Object.keys(group.controls).forEach(key => {
      const value = group.get(key);
      this.formsErrors[key] = '';
      if (value && !value.valid && (value.touched || value.dirty)) {
        const message = this.validationMessages[key];
        for (const error in value.errors) {
          if (error) {
            this.formsErrors[key] += message[error];
          }
        }
      }
    });
  }

  validateProperty(key: string): boolean {
    const property = this.service.formRegisterDataReactive.get(key);
    return (property.valid && (property.touched || property.dirty));
  }

  touchedOrDirty(key: string): boolean {
    const prop = this.service.formRegisterDataReactive.get(key);
    return (prop.touched || prop.dirty);
  }

  registerUser() {
    this.showLoading = true;
    this.service.postInfoRegister().subscribe(
      res => {
        this.showLoading = false;
        this.toastr.success('register successfully', 'Info');
        this.router.navigateByUrl('user/login');
      },
      err => {
        this.showLoading = false;
        this.toastr.error(err.error instanceof ProgressEvent ? 'Server error occured. Try again later' : err.error);
        console.log(err);
      });
  }

}
