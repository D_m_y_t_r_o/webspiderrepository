import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: []
})

export class AppComponent implements OnInit {

  title = 'AngularUI';
  isAuth: boolean;
  name: string;

  constructor(private router: Router) { }

  ngOnInit() {
    this.updateHeader();
  }

  updateHeader() {
    this.isAuth = localStorage.getItem('token') != null;
    if (this.isAuth) {
      this.name = localStorage.getItem('name');
    } else {
      this.name = '';
    }
  }

  onLogout() {
    localStorage.removeItem('token');
    this.updateHeader();
    this.router.navigateByUrl('user/login');
  }

}
