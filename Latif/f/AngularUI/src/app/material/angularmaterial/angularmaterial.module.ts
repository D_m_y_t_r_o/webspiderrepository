import {NgModule} from '@angular/core';  
import {MatInputModule, MatPaginatorModule, MatTableModule} from '@angular/material';  
  
@NgModule({  
  exports: [  
    MatInputModule,  
    MatPaginatorModule,  
    MatTableModule,  
  ]  
})  
export class AngularmaterialModule { }