import { Injectable, ViewChild, EventEmitter } from '@angular/core';
import { InfoDetail } from './info-detail.model';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { MatTableDataSource, MatPaginator } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class InfoDetailService {
  formData: InfoDetail;
  public onSubmit = new EventEmitter<any>();
  buttonName : string;
  
  readonly rootURL='https://localhost:44388/api';
  constructor(private http:HttpClient) { }

  postInfoDetail(formData:InfoDetail){
      return this.http.post(this.rootURL+'/taskInfo', this.formData);
  }


  putInfoDetail(): Observable<any>{
      return this.http.put(this.rootURL+'/taskInfo/'+this.formData.Id, this.formData);
  }

  deleteInfoDetail(id){
    return this.http.delete(this.rootURL+'/taskInfo/'+id)
  }
  
  AllInfoDetails(): Observable<InfoDetail[]> {  
    return this.http.get<InfoDetail[]>(this.rootURL + '/taskInfo')  
  }

}

