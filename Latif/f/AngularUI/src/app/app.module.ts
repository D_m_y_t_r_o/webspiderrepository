import { BrowserModule} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AngularmaterialModule } from './material/angularmaterial/angularmaterial.module'; 

import { AppComponent } from './app.component';
import { InfoDetailsComponent } from './info-details/info-details.component';
import { InfoDetailComponent } from './info-details/info-detail/info-detail.component';
import { InfoDetailListComponent } from './info-details/info-detail-list/info-detail-list.component';
import { InfoDetailService } from './shared/info-detail.service';
import 'hammerjs';

@NgModule({
  declarations: [
    AppComponent,
    InfoDetailsComponent,
    InfoDetailComponent,
    InfoDetailListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, 
    HttpClientModule,
    BrowserAnimationsModule,
    AngularmaterialModule, 
    ToastrModule.forRoot()
  ],
  providers: [InfoDetailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
