import { Component, OnInit } from '@angular/core';
import { InfoDetailService } from 'src/app/shared/info-detail.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-info-detail',
  templateUrl: './info-detail.component.html',
  styles: []
})
export class InfoDetailComponent implements OnInit {

  constructor(private service:InfoDetailService,
     private toastr: ToastrService) { }

  ngOnInit() {
    this.service.buttonName = "Submit";
    this.resetForm();
    
  }

  resetForm(form?:NgForm){
    if(form!=null)
    form.resetForm();
    this.service.formData = {
      Id:0,
      Link:null,
      XPath:null,
      TimeStart:null,
      Period:null
    }
  }

  //Button response
  onSubmit(form:NgForm){
    if(this.service.formData.Id == 0)
      this.insertRecord(form);
    else
      this.updateRecord(form);
  }
  
  //Add function 
  insertRecord(form: NgForm){
    this.service.postInfoDetail(form.value).subscribe(
      res =>{
        this.service.onSubmit.emit(null); 
        this.toastr.success('Inserted successfully', 'Info. ADDED');
        this.resetForm(form);
      }, 
      err=>{
        console.log(err);
      });  
  }

  //Update task function 
  updateRecord(form: NgForm){
    this.service.putInfoDetail().subscribe(
      res =>{
        this.service.buttonName = "Submit";
        this.service.onSubmit.emit(null);
        this.toastr.warning('Updated successfully', 'INF. ADDED');
        this.resetForm(form);
      },
      err=>{
        console.log(err);
      });
  } 
}
