import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { InfoDetailService } from 'src/app/shared/info-detail.service';
import { InfoDetail } from 'src/app/shared/info-detail.model';
import { ToastrService } from 'ngx-toastr';
import { MatTableDataSource, MatPaginator } from '@angular/material'; 

@Component({
  selector: 'app-info-detail-list',
  templateUrl: './info-detail-list.component.html',
  styles: [],

})
export class InfoDetailListComponent implements OnInit {

  public dataSource: MatTableDataSource<InfoDetail>;  
  public displayedColumns: string[] = ['Link', 'XPath', 'TimeStart', 'Period','Id'];  
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;   

  constructor(private service:InfoDetailService, 
    private toastr: ToastrService){} 


  ngOnInit() {
    this.service.onSubmit.subscribe(()=> 
    {
      this.updateTable();
    });
    this.updateTable();  
  }

  //Update table
  public updateTable(){
    this.service.AllInfoDetails().subscribe(data =>{  
      this.dataSource = new MatTableDataSource(data);  
      this.dataSource.paginator = this.paginator; 
    }); 
  }
  
  //Search filter
  applyFilter(filterValue: string) {  
    this.dataSource.filter = filterValue.trim().toLowerCase();  
    if (this.dataSource.paginator) {  
      this.dataSource.paginator.firstPage();  
    }  
  }

 //Select item and we can work with him  
  populateForm(inf :InfoDetail){
    this.service.formData = Object.assign({},inf);
    this.service.buttonName = "Update";
  }

  //Delete selected item
  onDelete(Id){
    if(confirm('Are you sure to delete this record ?'))
    this.service.deleteInfoDetail(Id)
    .subscribe(res =>{
      this.updateTable()
      this.toastr.warning('Deleted successfully', 'Info Detail')
    },
      err=>{
        console.log(err);
    })
  }
}
