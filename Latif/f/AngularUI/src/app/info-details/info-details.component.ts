import { Component, OnInit } from '@angular/core';
import { InfoDetailService } from '../shared/info-detail.service';

@Component({
  selector: 'app-info-details',
  templateUrl: './info-details.component.html',
  styles: []
})
export class InfoDetailsComponent implements OnInit {

  constructor(private service: InfoDetailService) { }

  ngOnInit() {}

}
